#! /bin/bash

# check params
makeTest=0
makeBuild=0
makeClean=0
showWarning=0
customMake=0
dossier=0
doc=0

for param in "$@"
do
    case "$param" in
        "--dossier")
            dossier=1
            customMake=1
            ;;
        "--test")
            makeTest=1
            customMake=1
            ;;
        "--build")
            makeBuild=1
            customMake=1
            ;;
	"--documentation")
	    doc=1
	    customMake=1
	    ;;
        "--build-debug")
            makeBuild=2
            customMake=1
            ;;
        "--clean")
            makeClean=1
            customMake=1
            ;;
        "--warning")
            showWarning=1
            ;;
        "--help")
            man ./build.sh.help
            exit
            ;;
        *)
            echo "WARNING : argument $param not recognized"
            echo ""
            ;;
    esac
done

if [ $doc -eq 1 ]
then
	echo "compiling the documentation..."
	cd doc
	doxygen Doxyfile
	cd latex
	make
	mv refman.pdf ../
	cd ..
        rm -r latex
        cd ..
        echo "doc compiled!"
fi

cd src
if [ ! -d "saves" ]
then
	wget "https://drive.google.com/uc?export=download&id=0B5z186U-o7YeQ25LR1pXenR4cmc" -O files.tar.gz
	tar xvzf files.tar.gz
	mv files/saves .
	mv files/mp3 .
	rmdir files
	rm files.tar.gz
fi
cd ..

   
# Default
if [ $customMake -eq 0 ]
then
    makeTest=1
    makeBuild=1
    dossier=1
fi

# test script
if [ $makeTest -eq 1 ]
then
    cd test
    
    echo "Compilation test/..."
    make 2>buildlog

    if [ $? -eq 0 ]
    then
        if [ $showWarning -eq 1 ]
        then
            if [ -s buildlog ]
            then
                echo "Des warnings sont apparus lors de la compilation, voir test/buildlog"
            else
                rm buildlog
            fi
        else
            rm buildlog
        fi
        echo "Compilation test/ terminé."
        echo ""
        
        echo "Lancement des tests..."
        ./testsX 2> output
        if [ -s output ]
        then
            echo "Tests échoués, voir test/output"
            exit
        else
            echo "Tests réussis."
            rm output
        fi
        
    else
        echo "Compilation test/ impossible, voir test/buildlog"
        exit
    fi

    cd ../
    if [ $makeBuild -eq 1 ]
    then
        echo ""
    fi
fi

# build script
if [ $makeBuild -eq 1 ]
then
    cd src
    
    echo "Compilation src/..."
    make 2>buildlog

    if [ $? -eq 0 ]
    then
        if [ $showWarning -eq 1 ]
        then
            if [ -s buildlog ]
            then
                echo "Des warnings sont apparus lors de la compilation, voir src/buildlog"
            else
                rm buildlog
            fi
        else
            rm buildlog
        fi
        echo "Compilation src/ terminé."
    else
        echo "Compilation src/ impossible, voir src/buildlog"
        exit
    fi
    
    cd ../

elif [ $makeBuild -eq 2 ]
then
    cd src
    
    echo "Compilation src/, mode debug..."
    make debug 2> buildlog
    
    if [ $? -eq 0 ]
    then
        if [ $showWarning -eq 1 ]
        then
            if [ -s buildlog ]
            then
                echo "Des warnings sont apparus lors de la compilation, voir src/buildlog"
            else
                rm buildlog
            fi
        else
            rm buildlog
        fi
        echo "Compilation src/ terminé."
    else
        echo "Compilation src/ impossible, voir src/buildlog"
    fi

    cd ../
fi

if [ $makeClean -eq 1 ]
then
    cd test
    
    echo "Nettoyage tests/..."
    make clean 2> buildlog

    cd ../src
    
    echo "Nettoyage src/..."
    make clean 2> buildlog
    
    cd ../
fi

if [ $dossier -eq 1 ]
then
    cd dossier
    echo "Compiling dossier"
    pdflatex -shell-escape dossier.tex
    bibtex dossier.aux
    pdflatex -shell-escape dossier.tex
    cd ../
fi
