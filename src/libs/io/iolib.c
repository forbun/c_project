#include "iolib.h"
/// returns the number of digits in an int
int nbdigits(int n){
         if(n == 0){
                return 1;
        }
        else{  
                return floor(log10(abs(n))) + 1;
        }
}
