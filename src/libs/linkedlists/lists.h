#ifndef LIST_H
#define LIST_H
#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

typedef struct treenode treenode;
struct treenode {
/**
this structure is not used yet
*/
	void* data;
	int maxleaf;///<number of leaves
	int maxfathers;
	treenode* leaves;///<array of leaves
	treenode* fathers;///<array of fathers;
};

/**
this structure is not used yet
*/
typedef struct ntree{
	treenode* root;
} ntree;

typedef struct linkedlist linkedlist;
typedef struct item item;

///this is the basic container for lists, stack and queues

/**to work correctly it needs a pointer to the structure you want to add to your list
to unlock further features you should also have a way to tell if any item is "bigger" or "smaller"
than any other*/
struct item{
	int number;///< item number in its current list
	void* data;///<pointer to the struct its referencing
	item* next;///<next item in list
	item* prev;///<prev item in list
};
typedef struct hashtable hashtable;

/// this structure is a wrapper to manipulate lists
struct linkedlist{
	item* element;///<in theory this is the first element of the list but can be any other
	size_t itemsize;///<size of the structure each item will reference in its data field
	int (*sup)(void*,void*);///<function pointers used to compare item data for sorting purposes
	int (*inf)(void*,void*);///<function pointers used to compare item data for sorting purposes
	hashtable* hashs;///< hashtable for analysis purposes
};

typedef struct stack{
	item* tip;///<item struct on top of the stack
	linkedlist* list;///<item list that contains all the items in the stack
}stack;

typedef struct queue{
	linkedlist* list;///<item list containing all items in queue
	item* head;///<pointer to first item in queue
	item* tail;///<pointer to last item in queue
	int prio;///<boolean, is this queue using a prioritization scheme?
}queue;

typedef struct hash hash;
struct hashtable{
	int size;///<size of hashtable
	hash* table;///< hash array
};

//PUBLIC
struct hash{
	item* hashed;///<pointer to the hashed item
	hash* next;///<pointer to next hash
};
stack* createstack(void*,size_t);
linkedlist* createlist(void *,void*,void*,size_t);
queue* createQueue(void*,void*,size_t);
item* additem(linkedlist*,void*);
item* getitem(linkedlist* liste,int number);
item* gettip(stack* monstack);
void swap(item*,item*);
item* insert(item*,int,void*,linkedlist*);
item* createItem(void*,size_t);
void* delitemByNum(linkedlist*,int);
int delitemByData(linkedlist*,void*,int);
void dellist(linkedlist**);
item* push(stack*, void*);
item* pop(stack*);
int Lempty(linkedlist*);
int Sempty(stack*);
void delstack(stack**);
item* enqueue(queue*,void*);
item* dequeue(queue*);
void delQueue(queue**);
int Qempty(queue*);
void listFusion(linkedlist**,linkedlist**);
void stackFusion(stack*,stack*);
linkedlist* listCpy(linkedlist*);
item* getfirst(linkedlist*);
item* getlast(linkedlist*);
stack* stackCpy(stack*);
queue* queueCpy(queue*);
linkedlist* listSplit(linkedlist* old,item*);
stack* stackSplit(stack*,item*);
void prioritize(queue*);
//private
void heapify(linkedlist*);
void renumber(item*,int);//renumber from item* counting from int
void queueFusion(queue*,queue*);//if the first one is prioritized then the fusion shall be
queue* queueSplit(queue*,item*);
void introsort(linkedlist*);
void sort(linkedlist*,item*,item*,int,int);
item* quickselect(linkedlist*,void*);
//private
item* recselect(linkedlist*, item*,item*,void*);
/*
//PRIVATE
hashtable* hashList(linkedlist*,int);
*/
#endif
