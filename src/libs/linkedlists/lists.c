#include "lists.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
linkedlist* createlist(void* data/**<[in]pointer to the data struct the item will reference*/,void* supfunc/**<[in]function to compare two items*/,void* infunc,size_t itemsize){
	linkedlist* mylist = (linkedlist*)malloc(sizeof(linkedlist));
	mylist->element = 0;
	mylist->sup = supfunc;
	mylist->inf = infunc;
	mylist->itemsize = itemsize;
	if(data != 0){
		additem(mylist,data);
	}
	mylist->hashs = 0;
	return mylist;
}


/**
This function returns an item pointer if the item number passed to it exists within the list, else it returns 0
*/
item* getitem(linkedlist* liste,int number){
	item* cur = liste->element;
	if(cur->number > number){
		while(cur->number != number && cur->prev != 0){
			cur = cur->prev;
		}
	}
	else if(cur->number < number){
		while(cur-> number != number && cur->next != 0){
			cur = cur->next;
		}
	}
	if(cur->number == number){
		return cur;
	}
	else{
		return 0;
	}
}

///returns an item* pointer after adding the void* data structure to the list
item* additem(linkedlist* liste,void* data){
	item* newitem = createItem(data,liste->itemsize);
	item* cur = liste->element;
	if(liste->element == 0){
		liste->element = newitem;
		newitem->prev = 0;
		newitem->next = 0;
		newitem->number = 0;
	}
	else{
		cur = getlast(liste);
		newitem->prev = cur;
		cur->next = newitem;
		newitem->number = cur->number+1;
	}
	return newitem;
}

///stack creation function, if passed 0 for data it will create an empty linkedlist
stack* createstack(void* data,size_t itemsize){
	stack* res = (stack*)malloc(sizeof(stack));
	assert(res != 0);
	res->list = createlist(data,0,0,itemsize);
	res->tip = res->list->element;
}

///return the item on top of the stack
item* gettip(stack* monstack){
	return monstack->tip;
}

///adds one item to the stack, data in said item passed as a pointer
item* push(stack* monstack, void* data){
	item* res = additem(monstack->list,data);
	monstack->tip = res;
	return res;
}

///exchange two items in a list, can exchange two items in different lists
void swap(item* a,item* b){
     int tmp;
     item* preva = a->prev;
     item* nexta = a->next;
     item* prevb = b->prev;
     item* nextb = b->next;
if(a!=b){
     if(nexta == b){
	b->prev = preva;
	b->next = a;
	if(nextb != 0){
		nextb->prev = a;
	}

	a->prev = b;
	a->next = nextb;
	if(preva != 0){
		preva->next = b;
	}
	     tmp = a->number;
	     a->number = b->number;
	     b->number = tmp;
     }
     else if(nextb == a){
	swap(b,a);
     }
     else{
	b->prev = preva;
	b->next = nexta;
	if(preva != 0){
		preva->next = b;
	}
	if(nexta != 0){
		nexta->prev = b;
	}
	a->prev = prevb;
	a->next = nextb;
	if(prevb != 0){
		prevb->next = a;
	}
	if(nextb != 0){
		nextb->prev = a;
	}
	     tmp = a->number;
	     a->number = b->number;
	     b->number = tmp;
}
}
}

///creates a new item not attached to any list
/** the new item will contain a pointer to data passed to it, beware that it makes
a deep copy of the data structure (hence the importance of the size parameter)*/
item* createItem(void* data,size_t itemsize){
	item* res = (item*)malloc(sizeof(item));
	void* newdata = (void*)malloc(itemsize);
	memcpy(newdata,data,itemsize);
	res->prev = 0;
	res->next = 0;
	res->data = newdata;
	return res;
}



item* insert(item* repere,int after/**<[in] set to 0 inserts before, 1 inserts after*/,void* data,linkedlist* mylist){//int = 0 insert before, int = 1 insert after

	item* newitem = createItem(data,mylist->itemsize);
	assert(newitem != 0);
	if(repere == 0){
		item* cur = getlast(mylist);
		repere = cur;
	}
	if(after){
		item* suiv = (repere->next);
		repere->next = newitem;
		if(suiv != 0){
			suiv->prev = newitem;
		}
		newitem->prev = repere;
		newitem->next = suiv;
		newitem->number = (repere->number)+1;
		while(suiv != 0){
			suiv->number = (suiv->number)+1;
			suiv = suiv->next;
		}
	}
	else{
		item* prev = (repere->prev);
		repere->prev = newitem;
		newitem->prev = prev;
		newitem->next = repere;
		if(repere->number >= 1){
			prev->next = newitem;
			newitem->number = (repere->number-1);
		}
		else{
			newitem->number = 0;
		}
		item* cur = repere;
		while(cur != 0){
			cur->number = cur->number+1;
			cur = cur->next;
		}

	}
	return newitem;
}

///deletes an item based on its number
	void* delitemByNum(linkedlist* mylist,int num){
		item* todel;
		item* prev;
		item* suiv;
		todel = getitem(mylist,num);
		prev = (todel->prev);
		suiv = (todel->next);
		void* data =(void*) malloc(mylist->itemsize);
		assert(memcpy(data,(todel->data),mylist->itemsize) != 0);
		free(todel);
		item* cur = suiv;
		if(prev!=0){
			prev->next = suiv;
			mylist->element = prev;
		
		}
		else{
			mylist->element = suiv;
		}
		while(cur != 0){
				cur->number = cur->number -1;
				cur = cur->next;
			}
		if(suiv != 0){
			suiv->prev = prev;
		}
		return data;
	}

	
///deletes a list, freeing its allocated memory and set the linkedlist pointer to 0
void dellist(linkedlist** tolist){
if(*tolist != 0){
	item* cur;
	item* nxt;
	linkedlist* mylist = *tolist;
	cur = mylist->element;
	if(cur != 0){
		if(cur->number != 0){
			cur = getfirst(mylist);
			}
		
		nxt = cur->next;
		while(nxt != 0){
			free(cur);
			cur = nxt;
			nxt = cur->next;
		}
	}
	free(mylist);
	*tolist = 0;
}
}


///retrieve the item on top of the stack, deleting the corresponding container
item* pop(stack* monstack){
	item* res = monstack->tip;
	if(monstack->tip != 0){
		monstack->tip = monstack->tip->prev;
		if(monstack->tip != 0){
			monstack->tip->next = 0;
		}
		return res;
	}
	return (void*) 0;

}

///boolean returns true if list is empty
int Lempty(linkedlist* mylist){
	return mylist->element == 0;
}

///boolean returns false if stack is empty
int Sempty(stack* monstack){
	return monstack->tip == 0;
}

///deletes the stack and free its allocated memory
void delstack(stack** monstack){
	stack* opstack = *monstack;
	if(opstack->tip != 0){
		linkedlist** llist = &(opstack->list);
		dellist(llist);
	}
	free(*monstack);
	*monstack = 0;
}

///creates a new queue
queue* createQueue(void* data/**<[in]pointer for item creation, set to 0 for empty queue*/,void* sup/**comparison function used for\
	prioritization, can be set to 0*/,size_t itemsize){
	queue* myQ = (queue*)malloc(sizeof(queue));
	myQ->list = createlist(data,sup,0,itemsize);
	myQ->head = myQ->list->element;
	myQ->tail = myQ->list->element;
	myQ->prio = 0;
	return myQ;
}

///ads item to queue
item* enqueue(queue* myq, void* data){
	item* newitem = additem(myq->list,data);
	myq->tail = newitem;
	if(myq->head == 0){
		myq->head = newitem;
	}
	if(myq->prio){
		heapify(myq->list);
	}
	return newitem;
}

///retrieve item in head of queue
item* dequeue(queue* myq){
	if(myq->head != 0){
	item* res = myq->head;
	myq->head = myq->head->next;
	res->next = 0;
	res->prev = 0;
	if(myq->head != 0){
		myq->head->prev = 0;
		renumber(myq->head,0);
		if(myq->prio){
			heapify(myq->list);
		}
	}
	else{
		myq->tail = 0;
	}
	return res;
	}
	return 0;
}

///deletes queue and free memory
void delQueue(queue** myq){
	queue* todel = *myq;
	dellist(&(todel->list));
	free(todel);
	*myq = 0;
}

///boolean returns true if queue empty
int Qempty(queue* myq){
	return(myq->head == myq->tail && myq->head == 0);
}

///fuse one list after the other
void listFusion(linkedlist** l1/**<[in,out]list on which you wish to glue the second one*/, linkedlist** l2/**<[in]list which shall disappear\
	and which content shall be appended to the first list*/){
	if(!Lempty(*l1) && !Lempty(*l2)){
		item* last =  getlast(*l1);
		item* first = getfirst(*l2);
		last->next = first;
		first->prev = last;//we take the last item of l1 and glue it to  l2
		while(first!=0){
			first->number = first->prev->number + 1;
			first = first->next;
		}
		(*l2)->element = 0;
	}
	else if(Lempty(*l1)){
		*l1 = listCpy(*l2);
		dellist(l2);
	}
}

///fuses two stacks, one on top of the other
void stackFusion(stack* firstack, stack* secondstack/**<[in]this stack tip shall become the tip of both*/){
	firstack->tip = secondstack->tip;
	listFusion(&(firstack->list),&(secondstack->list));
	delstack(&secondstack);
}

///returns first item of list, meaning the item which has no previous item
item* getfirst(linkedlist* mylist){
	item* cur = mylist->element;
	while(cur->prev != 0){
		cur = cur->prev;
	}
	return cur;
}

///returns last item in list, the item which has no next item
item* getlast(linkedlist* mylist){
	item* cur = mylist->element;
	while(cur->next != 0){
		cur = cur->next;
	}
	return cur;
}

///makes a deep copy of the list passed to it
linkedlist* listCpy(linkedlist* mylist){
	linkedlist* newlist = createlist(0,mylist->sup,mylist->inf,mylist->itemsize);
	if(!Lempty(mylist)){
		item* cur = getfirst(mylist);
		while(cur != 0){
			additem(newlist,cur->data);
			cur = cur->next;
		}
	}
	return newlist;
}

///makes a deep copy of the stack passed to it
stack* stackCpy(stack* oldstack){
	stack* newstack = createstack(0,oldstack->list->itemsize);
	newstack->list = listCpy(oldstack->list);
	newstack->tip = getlast(newstack->list);
	return newstack;
}

///makes a deep copy of the queue passed to it
queue* queueCpy(queue* old){
	queue* new = createQueue(0,old->list->sup,old->list->itemsize);
	new->list = listCpy(old->list);
	new->head = getfirst(new->list);
	new->tail = getlast(new->list);
	return new;
}

///splits one list in two
/** important, it should be noted that the split will occur at the passed item position which shall become
the new list first item*/
linkedlist* listSplit(linkedlist* old,item* tosplit/**<[in,out]first item of the new list*/){
	item* prev = tosplit->prev;
	old->element = getfirst(old);
	tosplit->prev = 0;
	prev->next = 0;
	linkedlist* newlist = createlist(0,old->sup,old->inf,old->itemsize);
	newlist->element = tosplit;
	tosplit->number = 0;
	tosplit = tosplit->next;
	while(tosplit!=0){
		tosplit->number = tosplit->prev->number +1;
		tosplit = tosplit->next;
	}
	return newlist;
}

///stack splitting function, the item passed to it shall become the base of the new stack
stack* stackSplit(stack* old,item* tosplit){
	stack* new = createstack(0,old->list->itemsize);
	old->tip = tosplit->prev;
	new->list = listSplit(old->list,tosplit);
	new->tip = getlast(new->list);
	return new;
}

/**make a heap from a list*/
void heapify(linkedlist* list){
	item* cur = getlast(list);
	int moved = 0;
	int i = cur->number;
	item* prev = cur->prev;
	while(cur != 0){
		if(list->sup(cur->data,getitem(list,cur->number/2)->data)){
			moved++;
			swap(cur,getitem(list,cur->number/2));
		}
		cur = prev;
		if(cur !=0){
			prev = cur->prev;
		}
	}
	if(moved != 0){
		heapify(list);
	}
}

///make a priority queue from a normal queue using the heapify function
void prioritize(queue* myq){
	heapify(myq->list);
	myq->head = getfirst(myq->list);
	myq->tail = getlast(myq->list);
	myq->prio = 1;
}

///renumber items from 0 in the list, used in case of error as a dirty hack
void renumber(item* elitem, int n){
	item* cur = elitem;
	int j = n;
	while(cur != 0){
		cur->number = j;
		j++;
		cur=cur->next;
	}
}

///combine two queues in one, if it is a prioritized queue the resulting shall be ordered in a heap
/**BEWARE it does not check wether both lists contain the same type of items!!!*/
void queueFusion(queue* old,queue* new){
	old->tail = new->tail;
	listFusion(&(old->list),&(new->list));
	new->list = 0;
	delQueue(&new);
	if(old->prio){
		heapify(old->list);
	}
}

///split queue at the passed item position
queue* queueSplit(queue* old,item* tem /**<[in] split the queue at that item, it will be the first of the new queue*/){
	queue* new = createQueue(0,old->list->sup,old->list->itemsize);
	new->list = listSplit(old->list,tem);
	new->head = getfirst(new->list);
	new->tail = getlast(new->list);
	old->head = getfirst(old->list);
	old->tail = getlast(old->list);
	if(old->prio){
		prioritize(old);
		prioritize(new);
	}
	return new;
}

///sorting function, it uses quicksort and heapsort, it requires a sup and inf func from the list it is applied to

/**it can segfault easily since it does not check wether those function pointers are set to 0*/
void sort(linkedlist* liste, item* tem1/**<[in]first item in the interval we are sorting*/, item* tem2/**<[out]last item we are going to sort*/,int numite/**<[in]current iteration number*/, int maxite/**<[in] max iteration at wich we start using the mean of means, usually 2*log(size of set)*/){
if(tem1!= tem2 && numite < maxite){
	item* pivot = tem2;
	int start = tem1->number;
	int stop = tem2->number;\
	int storindex = tem1->number;
	item* cur = tem1;
	item* tmp;
	while(cur != tem2){
		tmp = cur->next;
		if(liste->sup(cur->data,pivot->data)){
			swap(cur,getitem(liste,storindex));
			storindex++;
		}
		cur = tmp;
	}
	swap(pivot,getitem(liste,storindex));
	if(pivot->number ==start){
		sort(liste,pivot->next,getitem(liste,stop),numite+1,maxite);
	}
	else if(pivot->number== stop){
		sort(liste,getitem(liste,start),pivot->prev,numite+1,maxite);
	}
	else{
		sort(liste,getitem(liste,start),pivot->prev,numite+1,maxite);
		sort(liste,pivot->next,getitem(liste,stop),numite+1,maxite);
	}
	}
else if(numite >= maxite){
	heapify(liste);
}
	}

void introsort(linkedlist* mylist){
	//double maxite = 2 * log(getlast(mylist)->number + 1); lets use a magic number for now : 200
	sort(mylist,getfirst(mylist),getlast(mylist),0,200);
}

item* quickselect(linkedlist* list, void* data){
	//double maxite = 2 * log(getlast(mylist->number + 1))
	return recselect(list,getfirst(list),getlast(list),data);
}

/// recursive quickselect
/** this function uses quickselect and the mean of means in case it takes too long*/
item* recselect(linkedlist* list,item* tem1,item* tem2,void* data){
if(tem1 != tem2 && tem1 != 0 && tem2 != 0){
	item* pivot = tem2;
	int start = tem1->number;
	int stop = pivot->number;
	int storindex = tem1->number;
	item* cur = tem1;
	item* tmp;
	while(cur != pivot){
		tmp = cur->next;
		if(list->sup(cur->data,pivot->data)){
			swap(cur,getitem(list,storindex));
			storindex++;
		}
		cur = tmp;
	}
	swap(pivot,getitem(list,storindex));


	if(list->sup(pivot->data,data)){
		return recselect(list,pivot->next,getitem(list,stop),data);
	}
	else if(list->inf(pivot->data,data)){
		return recselect(list,getitem(list,start),pivot->prev,data);
	}
	else if(!list->sup(pivot->data,data) && ! list->inf(pivot->data,data)){
		return pivot;
	}// pivot equal data^
	else{
		return recselect(list,pivot->next,getitem(list,stop),data);
	}
}
else{
	return 0;
}
}

///delitem using a pointer to data, will del up to n items which value equals that of data
int delitemByData(linkedlist* list,void* data/**[in]value we are looking for to delete*/,int n/**[in]maximum number of items to delete*/){
	item* todel;
	item* testitem = getfirst(list);
	todel = quickselect(list,data);
	int deleted = 0;
		if(n == 0){
				while(todel != 0){
					deleted++;
					delitemByNum(list,todel->number);
					todel = quickselect(list,data);
					}
		}
		else{
				while(deleted  < n && todel != 0){
						deleted++;
						delitemByNum(list,todel->number);
						todel = quickselect(list,data);
			}
	
		}
	return deleted;
}
