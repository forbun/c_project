#include "options/options.h"
#include "sounds/sounds.h"
optstruct optstatus;///<global option variable, used to determine things like music status or colors
mus soundenv;///< global sound environment variable, what piece is being played and what game state requires that piece

int main(int argc, char** argv) {
	if(argc < 2){
		printf("enter option file name as argument\n");
		return(1);
	}
	else{
		retrieve_options(&optstatus,argv[1]);
		if(sound_init()){
			if(load_files(&soundenv, &optstatus)){
				play_music(&optstatus,&soundenv,INTRO);
				call_menu();
				return (0);
			}
		}
		else{
			return (1);
		}
	}
}

