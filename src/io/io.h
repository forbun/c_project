#ifndef IO_HEADER
#define IO_HEADER
#define FILENAME_SIZE 50
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <string.h>

#define L 10
#define XL 12
#define XXL 14

typedef struct grid grid;

struct grid{
	char* grid;///< tableau de taille size*size contenant des char
	int playing;///<grille actuellement utilisée?
	int size;///<taille de la grille
	void (*del)(grid**);///<pointeur vers la fonction de suppression de la grille
};

void save_grid(grid*,char**);
void load_grid(grid**,char*,int);
grid* copy_grid(grid*,grid*);
//private
grid* make_grid(int size);
void del_grid(grid** grille);
#endif
