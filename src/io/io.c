#include "io.h"

///renvoie un pointeur sur une grille vide, toute les cases initialisées à __
grid* make_grid(int size){
	grid* grille = (grid*)malloc(sizeof(grid));
	grille->size = size;
	grille->playing == 0;
	grille->grid = (char*)malloc(sizeof(char) * size * size);
	grille->del = &del_grid;
	int i,j;
        #pragma omp parallel for
	for(i = 0; i < grille->size;i++){
	        #pragma omp parallel for
		for(j = 0; j < grille->size;j++){
			grille->grid[i*grille->size+j] = '_';
		}
	}
	return grille;
}

///supprime une grille
/**cette fonction n'est appelée que via la structure grille, celle ci contient un pointeur de fonction idoine*/
void del_grid(grid** grille){
	free((*grille)->grid);
	free(*grille);
	*grille = 0;

}

	///chargement des grilles
void load_grid(grid** magrille/**<[in,out] double pointeur dont la valeur changera pour correspondre à la grille chargée*/,char* filename,int size){
	FILE* fp = fopen(filename,"r");
	*magrille = (grid*)malloc(sizeof(grid));
	(*magrille)->del = del_grid;//in memory location of the del function might have changed
	(*magrille)->size = size;
	(*magrille)->grid = (char*)malloc(size * size * sizeof(char));//idem pour l'emplacement du tableau de char
	fread((*magrille)->grid,sizeof(char),size*size,fp);
	fclose(fp);
}

///fonction de sauvegarde d'une grille dans un fichier

/**la taille de la grille n'est pas sauvegardée, le layout du dossier de sauvegarde contenant cette information*/
void save_grid(grid* grille, char **locFile){
	char mkcmd[100];
	int written;
	char* dirpath;
	FILE* fp;
	int custom = 0;
	switch(grille->size){
		case L:
			dirpath = "saves/L/";
			sprintf(mkcmd,"mkdir -p %s",dirpath);
			system(mkcmd);
			break;
		case XL:
			dirpath = "saves/XL/";
			sprintf(mkcmd,"mkdir -p  %s",dirpath);
			system(mkcmd);
			break;
		case XXL:
			dirpath = "saves/XXL/";
			sprintf(mkcmd,"mkdir -p %s",dirpath);
			system(mkcmd);
			break;
		default: 
			dirpath = "saves/custom/";
			written = sprintf(mkcmd,"mkdir -p %s",dirpath);
			sprintf(mkcmd+written,"%d/",grille->size);
			system(mkcmd);
			custom = 1;
			break;
	}
	time_t now = time(0);
	struct tm* t = localtime(&now);
	char filename[FILENAME_SIZE];
	written = sprintf(filename,"%s",dirpath);
	if(custom){
		written += sprintf(filename+written,"%d/",grille->size);//je cree un dossier qui contiendra toute les saves custom de cette taille
	}
	strftime(filename+written,FILENAME_SIZE-1,"%m%d%H%M%S",t);//je traduis la date en string pour nommer le fichier
	fp = fopen(filename,"w");
	if(fp == 0){
		printf("fp pointe sur null!!!\n");
		exit(1);
	}
	fwrite(grille->grid,sizeof(char)*grille->size*grille->size,1,fp);
	fclose(fp);
	
	if(locFile != 0) {
		*locFile = malloc(FILENAME_SIZE * sizeof(char));
		strcpy(*locFile, filename);
	}
}

grid* copy_grid(grid *dest, grid *src) {
	// Vars
	int i,j;
	int sizeG = src->size;
	
	// Malloc new grid ?
	if(dest == 0)
		dest = make_grid(sizeG);
	// Check if src & dest have the same size
	if(src->size != dest->size)
		return dest;
	
	#pragma omp parallel for
	for(i = 0; i < dest->size;i++){
		#pragma omp parallel for
		for(j = 0; j < dest->size;j++){
			dest->grid[i*sizeG+j] = src->grid[i*sizeG+j];
		}
	}
	
	return dest;
}