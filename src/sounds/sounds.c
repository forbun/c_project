#include "sounds.h"

///fonction d'initialisation sdl, charge sdl_mixer
int sound_init()
{
    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return 0;    
    }
    
    //Initialize SDL_mixer
    if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        return 0;    
    }
    return 1;
}

///cette fonction charge les fichiers de musique, elle est appelée à chaque fois que le chargement d'un nouveau fichier est requis
/**ATTENTION!!!
Cette fonction utilise lourdement l'infrastructure linux, en cas de port vers windows il faudra probablement la scraper entièrement
pour la remplacer
*/
int load_files(mus* music,optstruct* options){
		char path[250];
		char tmp[200];
		char cmd[] = "ls mp3/allmusics/ | sort -R | head -n 1";
		/*recupère un nom de fichier déterminé aléatoirement dans la liste complète des musiques, un dossier contenant des liens symboliques
		vers tous les fichiers mp3*/
		FILE* fp;
		int i;
		char cmd2[250];
		int written;
		char c;
	if(options->mstatus != RANDOM){
		switch(music->place){
			case INTRO:
				sprintf(cmd2,"%s","ls mp3/intro/ | sort -R | head -n 1");
				fp = popen(cmd2,"r");
				written = sprintf(path,"%s","mp3/intro/");
				i = 0;
				c = fgetc(fp);
				while(!feof(fp)){
					tmp[i] = c;
					c = fgetc(fp);
					if(c == '\n'){
						c = '\0';
					}
					i++;
				}
				sprintf(path+written,"%s",tmp);
				music->music = Mix_LoadMUS(path);
				break;

			case INGAME:
				sprintf(cmd2,"%s","ls mp3/ingame/ | sort -R | head -n 1");
				fp = popen(cmd2,"r");
				written = sprintf(path,"%s","mp3/ingame/");
				i = 0;
				c = fgetc(fp);
				while(!feof(fp)){
					tmp[i] = c;
					c = fgetc(fp);
					if(c == '\n'){
						c = '\0';
					}
					i++;
				}
				sprintf(path+written,"%s",tmp);
				music->music = Mix_LoadMUS(path);
				break;

			case OUTRO:
				
				music->music = Mix_LoadMUS("mp3/outro/sonic.mp3");
				break;
			default:
				music->music = Mix_LoadMUS("mp3/intro/monty.mp3");
		}
	}
	else{
		fp = popen(cmd,"r");
		///ouverture d'un pointeur sur FILE qui contient l'output de la commande saisie
		c;
		written;
		written = sprintf(path,"%s","mp3/allmusics/");
		i = 0;
		c = fgetc(fp);
		while(!feof(fp)){
			tmp[i] = c;
			c = fgetc(fp);
			if(c == '\n'){
				c = '\0';
			}
			i++;
		}
		sprintf(path+written,"%s",tmp);
		music->music = Mix_LoadMUS(path);
	}
	if( music->music == 0 ){
		return 0;    
	}
	else{
		return 1;
	}
}

/**
cette fonction joue une musique si la musique actuelle est terminée, elle est destinée à être appelée en boucle plusieurs fois
par secondes pour vérifier l'état de l'environment sonore. Elle reçoit en paramètre une structure option, la encore lors de son
implémentation la globalité de la structure options n'était pas encore déterminée d'ou cette redaction
*/
int play_music(optstruct* options/**<[in] variable globale au programme*/,mus* env,g_state place){
	if(options->mstatus == YES && Mix_PlayingMusic() == 0){
		env->place = place;
		load_files(env,options);
		Mix_PlayMusic( env->music, 0 );
	}
	else if (options->mstatus == RANDOM && Mix_PlayingMusic() == 0 ){
		load_files(env,options);
		Mix_PlayMusic( env->music,0);
	}
}

