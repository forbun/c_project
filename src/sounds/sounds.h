#ifndef SOUND_HEADERS
#define SOUND_HEADERS
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include "options.h"
typedef enum g_state{INTRO,INGAME,OUTRO} g_state;///<\enum g_state enumeration of the game states, used for music as of now

///structure containing the current piece being played as well as the game state it is playing for
typedef struct mus{
	Mix_Music* music;///< pointeur vers la structure correspondant à un fichier musical
	g_state place;///< etat du jeu au moment de l'appel
}mus;

int sound_init();
int load_files(mus* music,optstruct* options);
int play_music(optstruct* options,mus* env,g_state place);

extern mus soundenv;
extern optstruct optstatus;
#endif
