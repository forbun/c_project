#include "interface.h"
#define NB_FILES_IN_MENU 2000
#include "sounds.h"
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define MAX_FOLDER_NAME_LENGTH 256
#define xGrid 7 // offset for grid

///fonction du menu père
/**
cette fonction affiche un menu en utilisant ncurses, elle se charge de l'initialisation de ncurses et de l'appel de tous les sous menus
qui sont regroupés dans des fonctions séparées
*/
void call_menu() {
	setlocale(LC_ALL, ""); ///< enable UTF-8 characters
	grid* grille;
	initscr();      /**< initialize the curses library */
	start_color();		   ///< enable colors
	setcolors();
	if(has_colors() == FALSE)
		leave_game(&grille);
	keypad(stdscr, TRUE);  /**< enable keyboard mapping */
	raw();
	nonl();         /**< tell curses not to do NL->CR/NL on output */
	cbreak();
	noecho();
	
	init_history();
	attron(COLOR_PAIR(1));
	char* new = "Nouvelle partie";
	char* load = "Charger une partie sauvegardee";
	char* quit = "Quitter";
	if(!optstatus.intro_played){
		roll_credits(1);
		optstatus.intro_played = 1;
		attroff(A_BOLD);
	}
	char* choices[] = {
			  	new,
				load,
				quit,
			  };
	int n = ARRAY_SIZE(choices);
	ITEM** my_items = (ITEM**)malloc(sizeof(ITEM*)*(n+1));
	int i;
	for(i = 0; i < n;i++){
		my_items[i] = new_item(choices[i],"");
		switch(i){
			case 0:
				set_item_userptr(my_items[i],nouvelle_partie);
				break;
			case 1:
				set_item_userptr(my_items[i],if_load_grid);
				break;
			case 2:
				set_item_userptr(my_items[i],leave_game);
		}
	}
	my_items[n] = (ITEM *)NULL;
	MENU* my_menu = new_menu(my_items);
	WINDOW* my_menu_win = newwin(10, 40, 4, 4);
	keypad(my_menu_win, 1);
        set_menu_win(my_menu, my_menu_win);
        set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));
		set_menu_fore(my_menu, COLOR_PAIR(1) | A_REVERSE);
		set_menu_back(my_menu, COLOR_PAIR(1));
	set_menu_mark(my_menu, " * ");
	mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
	mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
	mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
	refresh();
	post_menu(my_menu);
	wrefresh(my_menu_win);
	ITEM* cur;
	void (*p)(grid**);
	int c;
	i = 1;
	while(i){
		play_music(&optstatus,&soundenv,INTRO);
		c = getch();
		switch(c){
			case KEY_DOWN:
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case KEY_RIGHT:
				cur = current_item(my_menu);
				endwin();
				p = item_userptr(cur);
				p(&grille);
				i = 0;
				break;
		}
		wrefresh(my_menu_win);
	}
	attroff(COLOR_PAIR(1));
	
	leave_menu:
endwin();
}

///fonction d'interface lors du jeu
void play(grid *g/**<[in] pointeur sur la grille que l'on va jouer*/) {
    // Vars
    int x,y;
    char sym = '_';
    char choice = '_';
    int check_code = 1;
    int nbr_annule = 0;
	int valid_undo = 1;
	grid_move arr;
	arr.location = malloc(sizeof(coord));
		arr.location->i = 0;
		arr.location->j = 0;
    
    int sizeG = g->size;
    
    // Initialization
    errcg = 0;
	tipgr = 0;
    
    while(is_fill(g) == 0) {
		play_music(&optstatus,&soundenv,INGAME);
        // Clear head
		werase(stdscr);
		print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
		
		// Mark a case
		mvwprintw(stdscr, xGrid - 6, COLS / 2 - 17, "Vous voici maintenant dans le jeu.");
		
		mvwprintw(stdscr, xGrid - 5, COLS / 2 - 18, "L'aide est disponible en tapant 'h'.");
		if(check_code == 0) {
			attron(COLOR_PAIR(20) | A_BOLD);
			mvwprintw(stdscr, xGrid - 3, COLS / 2 - 14 - strlen(errcg->msg)/2, "ERREUR - GRILLE NON VALIDE : %s", errcg->msg);
			attron(COLOR_PAIR(1));
			attroff(A_BOLD);
		}
		
		wrefresh(stdscr);
		
		arr = nav_grid(stdscr, xGrid, COLS / 2 - 2*sizeG, arr.location, g);
		
		// Apply the choice
		if((char)(arr.value) == 'h') {
			// Help page
			werase(stdscr);
			
			mvwprintw(stdscr, 4, COLS / 2 - 2*sizeG, "Pour vous déplacer, utiliser les flèches.");
			mvwprintw(stdscr, 5, COLS / 2 - 2*sizeG, "Entrer '0' ou '1' là où vous voulez.");
			
			mvwprintw(stdscr, 7,  COLS / 2 - 2*sizeG, "Liste des commandes :");
			mvwprintw(stdscr, 8,  COLS / 2 - 2*sizeG, "    'h' : affiche cette aide");
			mvwprintw(stdscr, 9,  COLS / 2 - 2*sizeG, "    'q' : quitter le jeu");
			mvwprintw(stdscr, 10, COLS / 2 - 2*sizeG, "    'r' : résout la grille");
			mvwprintw(stdscr, 11, COLS / 2 - 2*sizeG, "    's' : sauve la grille");
			mvwprintw(stdscr, 12, COLS / 2 - 2*sizeG, "    't' : donne un indice");
			mvwprintw(stdscr, 13, COLS / 2 - 2*sizeG, "    'z' : annule la dernière entrée");
			
			wrefresh(stdscr);
			
			getch();
		} else if(((char)(arr.value) == '0' || (char)(arr.value) == '1') && check_code == 1) {
			sym = (char)(arr.value);
			wrefresh(stdscr);
			if(*((g->grid)+(g->size)*(arr.location->i)+(arr.location->j)) == '_') {
				mark_case(arr.location->i,arr.location->j,g,sym,1,grid_history);
				print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
				wrefresh(stdscr);
			} else {
				attron(COLOR_PAIR(20) | A_BOLD);
				mvwprintw(stdscr, xGrid - 3, COLS / 2 - 14, "ERREUR - Case déja marquée");
				attron(COLOR_PAIR(1));
				attroff(A_BOLD);
				wrefresh(stdscr);
				
				getch();
				
			}
		} else if((char)(arr.value) == 't' && check_code == 1) {
			// Tip
			get_suggestion(g);
			// Print it
			attron(COLOR_PAIR(30) | A_BOLD);
			mvwprintw(stdscr, xGrid - 3, COLS / 2 - 11, "ASTUCE : %s", tipgr->msg);
			attron(COLOR_PAIR(1));
			attroff(A_BOLD);
			wrefresh(stdscr);
			
			print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
			
			getch();
			
			// Clear old tipgr
			if(tipgr != 0) {
				free(tipgr->msg);
				free(tipgr);
				tipgr = 0;
			}
			
		} else if ((char)(arr.value) == 'q') {
			// Quit game (maybe)
			choice = '_';
			werase(stdscr);
			
			mvwprintw(stdscr, xGrid - 5, COLS / 2 - 20, "Etes-vous sûr de vouloir quitter ? (o|N)");
			
			print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
			while(choice != 'o' && choice != 'n') {
				choice = (char)(getch());
			}
			if(choice == 'o') {
				endwin();
				leave_game(&g);
			}
		} else if ((char)(arr.value) == 'r') {
			// Resolve grid
			choice = '_';
			werase(stdscr);
			
			mvwprintw(stdscr, xGrid - 5, COLS / 2 - 15, "Voulez-vous la solution ? (o|N)");
			
			print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
			while(choice != 'o' && choice != 'n') {
				choice = (char)(getch());
			}
			if(choice == 'o') {
				solve_grid(g,0,grid_history);
				
				werase(stdscr);
				mvwprintw(stdscr, xGrid - 5, COLS / 2 - 9, "Voici la solution :");
				
				print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
				wrefresh(stdscr);
				
				getch();
				
				endwin();
				leave_game(&g);
			}
		} else if ((char)(arr.value) == 's') {
			// Save grid (only content, not history yet)
			char *fileLoc;	// Print location of new file
			
			werase(stdscr);
			print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
			wrefresh(stdscr);
			
			save_grid(g, &fileLoc);
			
			attron(COLOR_PAIR(40) | A_BOLD);
			mvwprintw(stdscr, xGrid - 5, COLS / 2 - 9, "Grille sauvegardée.");
			mvwprintw(stdscr, xGrid - 4, COLS / 2 - 13, "Fichier : %s", fileLoc);
			attron(COLOR_PAIR(1));
			attroff(A_BOLD);
			getch();
			
		} else if ((char)(arr.value) == 'z') {
			// Clear head
			werase(stdscr);
			
			mvwprintw(stdscr, xGrid - 5, COLS / 2 - 14, "Annulation dernière action...");
			valid_undo = unmark_case(g,grid_history);
			nbr_annule += valid_undo;
			if(valid_undo == 0) {
				attron(COLOR_PAIR(20) | A_BOLD);
				mvwprintw(stdscr, xGrid - 4, COLS / 2 - 14, "ERREUR - Impossible d'annuler");
				attron(COLOR_PAIR(1));
				attroff(A_BOLD);
			}
			print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
			
			mvwprintw(stdscr, xGrid - 2, COLS / 2 - 16, "Vous avez annulé une entrée %d fois", nbr_annule);
			
			wrefresh(stdscr);
			
			getch();
			
		}
		
		// Check new grid (logical issues, no choice for a case, no solution for the grid)
		check_code = check_grid(g);
		if(check_code == 1)
			check_code *= check_impossible_mark(g);
		if(check_code == 1)
			check_code *= check_resolvable_grid(g);
    }
    
    // End of the game
	werase(stdscr);
	print_grid_offset(stdscr, xGrid, COLS / 2 - 2*sizeG, g);
	mvwprintw(stdscr, xGrid - 5, COLS / 2 - 17, "Bravo, vous avez résolu la grille !");
	wrefresh(stdscr);
	
	getch();
    
    del_history();
	endwin();
	
	leave_game(&g);
}

/// Display the grid, doesn't move cursor
void print_grid(WINDOW *win, grid *g) {
    print_grid_offset(win,-1,-1,g);
}

/// Display the grid in static way, with a possibly offset
void print_grid_offset(WINDOW *win, int x, int y, grid *g) {
	// Vars
    int i,j;
    char *cTmp;
    
    int size = g->size;
    
    // Head
	attron(COLOR_PAIR(11));
	if(x == -1 && y == -1)
		wprintw(win, "\u250F");
	else
		mvwprintw(win,x,y, "\u250F");
	
    for (j = 0; j < size - 1; j++) {
        wprintw(win, "\u2501\u2501\u2501\u252F");
    }
    wprintw(win, "\u2501\u2501\u2501\u2513\n");
    
    for (i = 0, cTmp = g->grid; i < size - 1; i++) {
		if(x == -1 && y == -1)
			wprintw(win, "\u2503");
		else
			mvwprintw(win,x+2*i+1,y, "\u2503");
		
        for (j = 0; j < size - 1; j++, cTmp++) {
			set_grid_content_color(i,j);
			
            wprintw(win, " %c ", (*cTmp != '_' ? *cTmp : ' '));
			attron(COLOR_PAIR(11));
			attroff(A_REVERSE);
			wprintw(win, "\u2502");
        }
		set_grid_content_color(i,j);
		
		wprintw(win, " %c ", (*cTmp != '_' ? *cTmp : ' '));
		attron(COLOR_PAIR(11));
		attroff(A_REVERSE);
		wprintw(win, "\u2503\n");
        cTmp++;
        
		if(x == -1 && y == -1)
			wprintw(win, "\u2520");
		else
			mvwprintw(win,x+2*i+2,y, "\u2520");
		
        for (j = 0; j < size - 1; j++) {
            wprintw(win, "\u2500\u2500\u2500\u253C");
        }
        wprintw(win, "\u2500\u2500\u2500\u2528\n");
        
    }
    
    // Last line
    if(x == -1 && y == -1)
		wprintw(win, "\u2503");
	else
		mvwprintw(win,x+2*size-1,y, "\u2503");
	
	for (j = 0; j < size - 1; j++, cTmp++) {
		set_grid_content_color(i,j);
		
		wprintw(win, " %c ", (*cTmp != '_' ? *cTmp : ' '));
		attron(COLOR_PAIR(11));
		attroff(A_REVERSE);
		wprintw(win, "\u2502");
	}
    
	set_grid_content_color(i,j);
	
	wprintw(win, " %c ", (*cTmp != '_' ? *cTmp : ' '));
	attron(COLOR_PAIR(11));
	attroff(A_REVERSE);
	wprintw(win, "\u2503\n");
    cTmp++;
    
	if(x == -1 && y == -1)
		wprintw(win, "\u2517");
	else
		mvwprintw(win,x+2*size,y, "\u2517");
	
    for (j = 0; j < size - 1; j++) {
        wprintw(win, "\u2501\u2501\u2501\u2537");
    }
    wprintw(win, "\u2501\u2501\u2501\u251B\n");
	
	attron(COLOR_PAIR(1));
}

/// Display the grid in dynamic way, with a possibly offset

/// You can navigate into the grid
/// @return coord and key when pressed (other than arrwo keys)
grid_move nav_grid(WINDOW *win, int x, int y, coord *pt, grid *g) {
	int c = 'a'; // Key code with ncurses
	grid_move res;
	res.location = pt;
	int sizeG = g->size;

	// Base of the nav
	print_grid_offset(win,x,y,g);

	while(1) {
		wmove(stdscr, x+2*res.location->i+1, y+4*res.location->j+2);
		wrefresh(stdscr);
		
		c = getch();
		
		switch(c) {
			case KEY_UP:
				res.location->i -= (res.location->i > 0 ? 1 : 0);
				break;
			case KEY_DOWN:
				res.location->i += (res.location->i < sizeG - 1 ? 1 : 0);
				break;
			case KEY_LEFT:
				res.location->j -= (res.location->j > 0 ? 1 : 0);
				break;
			case KEY_RIGHT:
				res.location->j += (res.location->j < sizeG - 1 ? 1 : 0);
				break;
			default:
				res.value = c;
				return res;
				break;
		}
	}
	
	return res;
}

///new game menu
void nouvelle_partie(grid** magrille){
	grid *grille = *magrille;
	
	char* choices[] = {
				"Go Back",
				"L",
				"XL",
				"XXL",
				"custom",
			};
	int n = ARRAY_SIZE(choices);
	int i;
	ITEM** my_items = (ITEM**)malloc(sizeof(ITEM*)*(n+1));
	for(i = 0; i < n;i++){
		my_items[i] = new_item(choices[i],"");
	}
	my_items[n] = (ITEM *)NULL;
	MENU* my_menu = new_menu(my_items);
	WINDOW* my_menu_win = newwin(10, 40, 4, 4);
	keypad(my_menu_win, 1);
        set_menu_win(my_menu, my_menu_win);
        set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));
		set_menu_fore(my_menu, COLOR_PAIR(1) | A_REVERSE);
		set_menu_back(my_menu, COLOR_PAIR(1));
	set_menu_mark(my_menu, " * ");
	mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
	mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
	mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
	refresh();
	post_menu(my_menu);
	wrefresh(my_menu_win);
	int c;
	int choixRec;
	int choixPlay;
	ITEM* cur;
	int sizeG;
	int done = 0;
	i = 1;
	while(done == 0){
		c = getch();
		switch(c){
			case KEY_DOWN:
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case KEY_RIGHT:
				cur = current_item(my_menu);
				switch(item_index(cur)) {
					case 0: // Go Back
						call_menu();
						break;
					
					case 1: // L : 10x10
						grille = make_grid(L);
						fill_grid(grille);
						done = 1;
						break;
					
					case 2: // XL : 12x12
						grille = make_grid(XL);
						fill_grid(grille);
						done = 1;
						break;
					
					case 3: // XXL : 14x14
						grille = make_grid(XXL);
						fill_grid(grille);
						done = 1;
						break;
					
					case 4: // Custom : Not yet...
						break;
				}
				break;
		}
		wrefresh(my_menu_win);
	}

	wrefresh(stdscr);
	// Enregistrement grille
	werase(stdscr);
	
	print_grid_offset(stdscr, xGrid, COLS / 2 - 2*grille->size, grille);
	mvwprintw(stdscr, xGrid - 5, COLS / 2 - 22, "voulez vous enregistrer cette grille ? (O/n)");
	wrefresh(stdscr);
	choixRec = 'a';
	while(choixRec != 'o' && choixRec != 'n') {
		choixRec = getch();
	}
	if(choixRec == 'o') {
		save_grid(grille,0);
	}

	// Jouons !
	werase(stdscr);
	print_grid_offset(stdscr, xGrid, COLS / 2 - 2*grille->size, grille);
	
	mvwprintw(stdscr, xGrid - 5, COLS / 2 - 14, "Voulez-vous jouer ou laisser");
	mvwprintw(stdscr, xGrid - 4, COLS / 2 - 19, "l'ordinateur resoudre la grille ? (P/c)");
	wrefresh(stdscr);
	choixPlay = 'a';
	while(choixPlay != 'p' && choixPlay != 'c') {
		choixPlay = getch();
	}
	if(choixPlay == 'p') {
		play(grille);
	} else {
		solve_grid(grille,0,grid_history);
		
		werase(stdscr);
		
		print_grid_offset(stdscr, xGrid, COLS / 2 - 2*grille->size, grille);
		mvwprintw(stdscr, xGrid - 5, COLS / 2 - 9, "Voici la solution :");
		wrefresh(stdscr);
		
		getch();
		
		endwin();
		leave_game(&grille);
	}
	

/* TODO custom case
 *			default:
				printf("entrez la taille de la grille que vous souhaitez utiliser:\n\
					0 - L 10*10\n\
					1 - XL 12*12\n\
					2 - XXL 14*14\n\
					3 - custom\n");
				scanf("%d",&choix);
				getchar();
				continue;
		}
	}
*/
}

///grid filling function, asks for input from the player to prepare a playing grid
void fill_grid(grid* grille){
	grid_move arr, arrTmp;
	arr.location = malloc(sizeof(coord));
		arr.location->i = 0;
		arr.location->j = 0;
	char cOld;
	char cNew;
	char *casePtr;
	
	werase(stdscr);
	refresh();
	
	while(1) {
		werase(stdscr);
		wrefresh(stdscr);

		mvwprintw(stdscr, xGrid - 6, COLS / 2 - 22, "Déplacez-vous avec les flèches, puis tapez la");
		mvwprintw(stdscr, xGrid - 5, COLS / 2 - 22, "valeur souhaitée. Les valeurs posibles sont :");
		mvwprintw(stdscr, xGrid - 4, COLS / 2 - 7 , "' ' / '0' / '1'");
		
		mvwprintw(stdscr, xGrid - 2, COLS / 2 - 17, "Pour terminer, appuyer sur Entrée.");
		
		arr = nav_grid(stdscr, xGrid, COLS / 2 - 2*grille->size, arr.location, grille);
		
		refresh();
		
		// Change *casePtr
		casePtr = (grille->grid)+((grille->size)*arr.location->i+arr.location->j);
		
		if((char)(arr.value) == ' ' || (char)(arr.value) == '0' ||
			(char)(arr.value) == '1') {
			*casePtr = (char)(arr.value);
		} else if ((int)(arr.value) == 13) { // Enter key (TODO)
			werase(stdscr);
			wrefresh(stdscr);
			
			print_grid_offset(stdscr, xGrid, COLS / 2 - 2*grille->size, grille);
			
			if(check_grid(grille) == 1) {
				mvwprintw(stdscr, xGrid - 5, COLS / 2 - 21, "Avez-vous fini de remplir la grille ? (o/N)");
				refresh();
				
				cOld = 'a';
				while(cOld != 'o' && cOld != 'n') {
					cOld = getch();
				}
				if(cOld == 'o') {
					werase(stdscr);
					wrefresh(stdscr);
					return;
				}
			} else {
				mvwprintw(stdscr, xGrid - 5, COLS / 2 - 22, "LA GRILLE N'EST PAS VALIDE, MERCI DE VERIFIER");
				refresh();
				
				getch();
			}
			
			
		}
	}
}

///non ncurses grid display function
void show_grid(grid* grille){
	int i,j;
	for(i = 0; i < grille->size;i++){
		for(j = 0; j < grille->size;j++){
			printf("%c ",grille->grid[i * grille->size + j]);
		}
		printf("\n");
	}
}

/// obtain a suggestion as coordinates and value from the solve_grid algorithm
void get_suggestion(grid* g){
	grid_move* next = (grid_move*)malloc(sizeof(grid_move));
	next->location = (coord*)malloc(sizeof(coord));
	solve_grid(g,next,grid_history);
	
	// Clear old tipgr
	if(tipgr != 0) {
		free(tipgr->msg);
		free(tipgr);
		tipgr = 0;
	}
	
	// Create & edit new tipgr
	tipgr = malloc(sizeof(tip_grid));
	tipgr->msg = malloc(100 * sizeof(char));
	
	tipgr->x = next->location->i;
	tipgr->y = next->location->j;
	sprintf(tipgr->msg, "%d en (%d,%d)", next->value, tipgr->x, tipgr->y);
}

///save loading menu
void if_load_grid(grid** grille){
	char* choices[] = {
				"Go Back",
				"L",
				"XL",
				"XXL",
				"custom",
			};
	int n = ARRAY_SIZE(choices);
	int size = 0;
	char* csize;
	int i;
	ITEM** my_items = (ITEM**)malloc(sizeof(ITEM*)*(n+1));
	for(i = 0; i < n;i++){
		switch(i){
			case 1:
				size = L;
				break;
			case 2:
				size = XL;
				break;
			case 3:
				size = XXL;
				break;
		}
		csize = (char*)malloc(sizeof(char)*nbdigits(size));
		sprintf(csize,"%d",size);
		my_items[i] = new_item(choices[i],csize);
	}
	my_items[n] = (ITEM *)NULL;
	MENU* my_menu = new_menu(my_items);
	WINDOW* my_menu_win = newwin(10, 40, 4, 4);
	keypad(my_menu_win, 1);
        set_menu_win(my_menu, my_menu_win);
        set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));
		set_menu_fore(my_menu, COLOR_PAIR(1) | A_REVERSE);
		set_menu_back(my_menu, COLOR_PAIR(1));
	set_menu_mark(my_menu, " * ");
	mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
	mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
	mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
	refresh();
	post_menu(my_menu);
	wrefresh(my_menu_win);
	int c;
	ITEM* cur;
	i = 1;
	char path[1035];
	char active_dir[1024] = "";
	char cmd[2048];
	int written;
	int row,col;
	int j;
	while(i){
		play_music(&optstatus,&soundenv,INTRO);
		c = getch();
		switch(c){
			case KEY_DOWN:
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case KEY_RIGHT:
				cur = current_item(my_menu);
				if(item_index(cur) == 0){
					endwin();
					call_menu();
				}
				else{
					FILE* fp = popen("pwd","r");
					fgets(active_dir,sizeof(active_dir)-1,fp);
					j=0;
					while(j < sizeof(active_dir)-1){
						if(active_dir[j] == '\n'){
							active_dir[j] = '\0';
							break;
						}
						j++;
					}
					written = sprintf(path,"%s",active_dir);
					written+=sprintf(path+written,"/%s","saves");
					written+=sprintf(path+written,"/%s",item_name(current_item(my_menu)));
					sprintf(cmd,"ls %s",path);
					fp = popen(cmd,"r");
					endwin();
					
					if(item_name(cur) == "custom"){
						*grille=folders_in_custom_menu(fp,path);
					}
					else{
						*grille = loadable_files_menu(fp,path,atoi(item_description(current_item(my_menu))));
					}
					werase(my_menu_win);
					wrefresh(my_menu_win);
					
					if(*grille == 0){
						goto endloop;
					} else {
						play(*grille);
					}
				}
				break;
		}
		endloop:
		wrefresh(my_menu_win);
	}
}

///quit function, displays credits
void leave_game(grid** g){
	Mix_HaltMusic();
	roll_credits(0);
	endwin();
	exit(0);
}

///displays all the files that can be loaded from the "saves" folder
grid* loadable_files_menu(FILE* fp,char* prefix,int size){
	linkedlist* choices = createlist(0,0,0,sizeof(char*));
	grid* res;
	int written=0;
	char sample[] = "0000000000";
	char** stringlist = (char**)malloc(sizeof(char*)*NB_FILES_IN_MENU);
	char d;
	int i = 0;
	int j = 0;
	stringlist[i] = (char*)malloc(sizeof(char)*strlen(sample));
	stringlist[i] = "Go Back";
	i++;
	stringlist[i] = (char*)malloc(sizeof(char)*strlen(sample));
	char* tmp;
	while(!feof(fp)){
		d = fgetc(fp);
		if(j == strlen(sample)){
			i++;
			stringlist[i] = (char*)malloc(sizeof(char)*strlen(sample));
			if(d = '\n'){
				d = fgetc(fp);
			}
			j = 0;
		}
		tmp = stringlist[i];
		tmp[j] = d;
		j++;
	}
	int n = i;
	ITEM** my_items = (ITEM**)malloc(sizeof(ITEM*)+(sizeof(ITEM*)*n));
	for(i = 0; i < n;i++){
		my_items[i] = new_item(stringlist[i],"");
	}
	my_items[n] = (ITEM*)NULL;
	MENU* my_menu = new_menu(my_items);
	WINDOW* my_menu_win = newwin(10, 40, 4, 4);
	keypad(my_menu_win, 1);
	set_menu_win(my_menu, my_menu_win);
	set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));
	set_menu_fore(my_menu, COLOR_PAIR(1) | A_REVERSE);
	set_menu_back(my_menu, COLOR_PAIR(1));
	set_menu_format(my_menu,6,1);
	set_menu_mark(my_menu, " * ");
	mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
	mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
	mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
	refresh();
	post_menu(my_menu);
	wrefresh(my_menu_win);
	char filename[250] = "";
	ITEM* cur;
	int c;
	while(1){
		c = getch();
		switch(c){
			case KEY_DOWN:
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case KEY_RIGHT:
				cur = current_item(my_menu);
				if(item_name(cur) == "Go Back"){
					return 0;
				}
				written = sprintf(filename,"%s",prefix);
				written += sprintf(filename+written,"/%s",item_name(cur));
				load_grid(&res,filename,size);
				endwin();
				return res;
				break;
		}
		wrefresh(my_menu_win);
	}
}

/**custom menu used for the custom folder which has one intermediate level in its hierarchy comprised of folders
named after the size of the grids inside them (eg the folder named "10" will contain grids 10 square wide)*/
grid* folders_in_custom_menu(FILE* fp, char* path){
	grid* res;
	linkedlist* choices = createlist(0,0,0,sizeof(char*));
	int written=0;
	char** stringlist = (char**)malloc(sizeof(char*)*NB_FILES_IN_MENU);
	char d;
	int i = 0;
	int j = 0;
	stringlist[i] = (char*)malloc(sizeof(char)*MAX_FOLDER_NAME_LENGTH);
	stringlist[i] = "Go Back";
	i++;
	stringlist[i] = (char*)malloc(sizeof(char)*MAX_FOLDER_NAME_LENGTH);
	char* tmp;
	while(!feof(fp)){
		d = fgetc(fp);
		if(d == '\n'){
			i++;
			stringlist[i] = (char*)malloc(sizeof(char)*MAX_FOLDER_NAME_LENGTH);
			d = fgetc(fp);
			j = 0;
		}
		tmp = stringlist[i];
		tmp[j] = d;
		j++;
	}
	int n = i;
	ITEM** my_items = (ITEM**)malloc(sizeof(ITEM*)+(sizeof(ITEM*)*n));
	for(i = 0; i < n;i++){
		my_items[i] = new_item(stringlist[i],"");
	}
	my_items[n] = (ITEM*)NULL;
	MENU* my_menu = new_menu(my_items);
	WINDOW* my_menu_win = newwin(10, 40, 4, 4);
	keypad(my_menu_win, 1);
	set_menu_win(my_menu, my_menu_win);
	set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));
	set_menu_fore(my_menu, COLOR_PAIR(1) | A_REVERSE);
	set_menu_back(my_menu, COLOR_PAIR(1));
	set_menu_format(my_menu,6,1);
	set_menu_mark(my_menu, " * ");
	mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
	mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
	mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
	refresh();
	post_menu(my_menu);
	wrefresh(my_menu_win);
	char filename[250] = "";
	ITEM* cur;
	FILE* newfp;
	char newcmd[250];
	char newpath[250];
	int c;
	while(1){
		c = getch();
		switch(c){
			case KEY_DOWN:
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case KEY_RIGHT:
				cur = current_item(my_menu);
				if(item_name(cur) == "Go Back"){
					return 0;
				}
				written = sprintf(newpath,"%s",path);
				written += sprintf(newpath+written,"/%s",item_name(cur));
				sprintf(newcmd,"ls %s",newpath);
				newfp = popen(newcmd,"r");
				res=loadable_files_menu(newfp,newpath,atoi(item_name(cur)));
				if(res == 0){
					goto endloop;
				}
				pclose(newfp);
				endwin();
				return res;
				break;
		}
		endloop:
		wrefresh(my_menu_win);
	}
}

///fonction d'affichage des credits
/**
	cette fonction recupère l'ascii art et les credits sauvegardés dans le dossier ascii_art
	avant d'utiliser ncurses et printw pour les faire défiler à l'écran
*/
void roll_credits(int type/**<[in] booleen, 0 credits de fin, 1 credits de debug*/){
	FILE* fp;
	char tmp[250];
	halfdelay(1);		   // No-blocking getch()
	if(type){
		fp = fopen("ascii_art/intro","r");
		play_music(&optstatus,&soundenv,INTRO);
	}
	else{
		fp = fopen("ascii_art/outro","r");
	}
	int row,col;
	attron(COLOR_PAIR(50) | A_BOLD);
	scrollok(stdscr,1);
	fgets(tmp,sizeof(tmp)-1,fp);
	while(!feof(fp)){
		if(getch() != ERR) {
			// Stop credits
			cbreak();	// Disable half-delay mode
			werase(stdscr);
			scrollok(stdscr,0);
			wrefresh(stdscr);
			return;
		}
		play_music(&optstatus,&soundenv,OUTRO);
		getmaxyx(stdscr,row,col);
		mvprintw(row-1,((col-strlen(tmp))/2),"%s",tmp);
		nanosleep((struct timespec[]){{0, 1000000}}, NULL);
		refresh();
		fgets(tmp,sizeof(tmp)-1,fp);
	}
	sleep(3);
	attron(COLOR_PAIR(1));
	attroff(A_BOLD);
	int i;
	for(i = 0; i < 500;i++){
		printw("							\n");
		refresh();
	}
	scrollok(stdscr,0);
	cbreak(); // Disable half-delay mode
}

///color settings, hardcoded part
void setcolors() {
	init_pair(  1, COLOR_WHITE, COLOR_BLACK);	// Default
	
	init_pair( 10, COLOR_CYAN , COLOR_BLACK);	// Grid, content
	init_pair( 11, COLOR_WHITE, COLOR_BLACK);	// Grid, border

	init_pair( 20, COLOR_RED  , COLOR_BLACK);	// Error, generic
	init_pair( 30, COLOR_CYAN , COLOR_BLACK);	// Info, generic
	init_pair( 40, COLOR_GREEN, COLOR_BLACK);	// OK, generic
	
	init_pair( 50, COLOR_CYAN , COLOR_BLACK);	// Credits
}

/// Sub-function for print_grid_offset : set color for the case (x:y), depends of
/// both global vars errcg and tipgr
void set_grid_content_color(int x, int y) {
	// Color in red reversed if in the rectangle define by errcg
	if(errcg != 0 && (
		x >= errcg->xBeg && x <= errcg->xEnd &&
		y >= errcg->yBeg && y <= errcg->yEnd
	)) {
		attron(COLOR_PAIR(20) | A_REVERSE);
	} else if(tipgr != 0 && (
		x == tipgr->x && y == tipgr->y
	)){
		attron(COLOR_PAIR(30) | A_REVERSE);
	} else {
		attron(COLOR_PAIR(10));
	}
}
