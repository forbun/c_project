#ifndef INTERFACE_H
#define INTERFACE_H
#define _X_OPEN_SOURCE_EXTENDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <locale.h>
#include <signal.h>
#include <menu.h>
#include <assert.h>
#include <errno.h>
#include <ncursesw/ncurses.h>
#include <time.h>

#include "sounds.h"
#include "io.h"
#include "grille.h"
#include "iolib.h"
extern mus soundenv;
extern optstruct optstatus;
void call_menu();

void play(grid *g);

void print_grid(WINDOW *win, grid *g);
void print_grid_offset(WINDOW *win, int x, int y, grid *g);
grid_move nav_grid(WINDOW *win, int x, int y, coord *pt, grid *g);

void roll_credits(int);//0 = intro 1 = outro
void show_grid(grid*);

void fill_grid(grid*);

void get_suggestion(grid*);

/** Private
les fonctions ci dessous ne sont à utiliser que d'une manière interne au fichier.c correspondant, elles sont
susceptibles de changer et d'etre réécrites
*/
void setcolors();
void nouvelle_partie(grid**);
void if_load_grid(grid**);
void leave_game(grid**);
grid* loadable_files_menu(FILE* fp,char* prefix,int size);
grid* folders_in_custom_menu(FILE* fp, char* path);
void set_grid_content_color(int x, int y);
#endif // INTERFACE_H
