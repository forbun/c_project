#include "grille.h"

///boolean returns a 1 if grid is correct, return 0 otherwise
int check_grid(grid *g) {
    // ------ Vars ------
    int i,j;
    int size = g->size;
    char *cTmp;
    
    // clear old errcg
    if(errcg != 0) {
        free(errcg->msg);
        free(errcg);
        errcg = 0;
    }
    
    checkGrid_vars cgv;
    // Init cgv
    cgv.nbr0inARow = 0;
    cgv.nbr0inLine = 0;
    cgv.nbr1inARow = 0;
    cgv.nbr1inLine = 0;
    cgv.hashLines = malloc(size * sizeof(int));
    #pragma omp parallel for
    for(i = 0; i < size; i++) {
        *(cgv.hashLines + i) = 0;
    }

    // ------ Look into grid ------
    // Horizontal
    #pragma omp parallel for
    for(i = 0, cTmp = g->grid; i < size; i++) {
        #pragma omp parallel for
        for (j = 0; j < size; j++, cTmp++) {
            if(check_this_case(*cTmp, &cgv, size, i, j, 1) == 0) {
                return 0;
            }
        }
        if(check_this_line(&cgv, size, i, 1) == 0) {
            return 0;
        }
        
        // Re-init vars
        cgv.nbr0inARow = 0;
        cgv.nbr0inLine = 0;
        cgv.nbr1inARow = 0;
        cgv.nbr1inLine = 0;
        
    }
    
    // Vertical
    #pragma omp parallel for
    for(i = 0; i < size; i++) {
        *(cgv.hashLines + i) = 0;
    }
    
    #pragma omp parallel for
    for(j = 0, cTmp = g->grid; j < size; ++j, cTmp = g->grid + j) {
        #pragma omp parallel for
        for (i = 0; i < size; i++, cTmp+=size) {
            if(check_this_case(*cTmp, &cgv, size, j, i, 0) == 0) {
                return 0;
            }
        }
        if(check_this_line(&cgv, size, j, 0) == 0) {
            return 0;
        }
        
        // Re-init vars
        cgv.nbr0inARow = 0;
        cgv.nbr0inLine = 0;
        cgv.nbr1inARow = 0;
        cgv.nbr1inLine = 0;
        
    }
    
    // ------ Clean ressources ------
    free(cgv.hashLines);
    
    // All passed : grid OK !
    return 1;
}

///boolean, is the grid completely filled?
int is_fill(grid *g) {
    coord c = first_empty_case(g);
    
    return (c.i == -1 && c.j == -1);
}

/// Return the coord of the first empty case (horizontal read) in a grid
coord first_empty_case(grid *g) {
    // Vars
    coord res;
    
    int i,j;
    char *cTmp;
    int size = g->size;
    
    // Look into grid
    #pragma omp parallel for
    for(i = 0, cTmp = g->grid; i < size; i++) {
        #pragma omp parallel for
        for (j = 0; j < size; j++, cTmp++) {
            if(*cTmp == '_') {
                res.i = i;
                res.j = j;
                return res;
            }
        }
    }
    
    // No cell with '_' : grid filled !
    res.i = -1;
    res.j = -1;
    return res;
}

/// Handles grid_history
/**it creates a root item that is never used*/
int init_history() {
    item_history item;
    
    item.x = -1;
    item.y = -1;
    item.isLogical = 1;
    item.cOld = 'z';
    item.cNew = 'z';
    
    grid_history = createstack(&item, sizeof(item_history));
    
    return 0;
}

/// Add in item in a history stack
int push_history(stack *histo, item_history h) {
    push(histo, &h);
    
    return 0;
}

/// Return the last mark in history
item_history last_mark() {
    return (*((item_history*)(grid_history->tip->data)));
}

/// Del top item in a history stack, then return it
item_history pop_history(stack *histo) {
    item *container = pop(histo);
    
    item_history h = *((item_history*)(container->data));
    return h;
}

/// Free stack history
int del_history() {
    delstack(&grid_history);
    grid_history = 0;
    
    return 0;
}

// Private

/// Set a case of the grid, then update the history
void mark_case(int x, int y, grid *g, char val, int isLogical, stack *histo) {
    item_history h;
    h.x = x;
    h.y = y;
    h.isLogical = isLogical;
    h.cOld = *((g->grid)+(g->size)*x+y);
    h.cNew = val;
    
    *((g->grid)+(g->size)*x+y) = val;
    push_history(histo, h);
}

/// Undo last mark of the grid and update the history

/// It returns 0 if no rollbask, 1 otherwise (increments).
int unmark_case(grid *g, stack *histo) {
    item_history h;
    
    h = pop_history(histo);
    
    if(h.x == -1 && h.y == -1) {
        push_history(histo,h);
        
        return 0;
    } else {
        int x = h.x;
        int y = h.y;
        char val = h.cOld;

        *((g->grid)+(g->size)*x+y) = val;
        
        return 1;
    }
}

/// Force mark of the first emty case of the grid

/// This mark can be recognized in the history with the value of isLogical.
void mark_guess(grid *g, stack *histo) {
    coord cEmpty = first_empty_case(g);
    int newVal;
    
    mark_case(cEmpty.i, cEmpty.j, g, '0', 0, histo);
}

/// Clear grid and history until the last force mark, then change the value.

/// @remark The new mark is now logic, because the binero accept only 2 values.
int unmark_guess(grid *g, stack *histo) {
    // Vars
    int newVal;
	int undo_code;
    item_history h;
    
    while(last_mark().isLogical != 0) {
        undo_code = unmark_case(g,histo);
		if(undo_code == 0)
			return 0;
    }
    
    // new mark
    h = last_mark();
    unmark_case(g,histo);
    newVal = (h.cNew == '0' ? '1' : '0');
    mark_case(h.x, h.y, g, newVal, 1,histo);
	
	return 1;
}

///this function will solve the grid using a simple set of rules
/** those rules can be found in any documentation regarding the game of binero
the algorithm looks for tuples of two or three items disposed in a manner that allows
the deduction of the value the surrounding squares should hold*/
void solve_grid(grid* g,grid_move* nextmove,stack *histo){
	if(histo == 0){
		init_history();
	}
	int suggest = nextmove != 0;
	item_history move;
	coord cur;
	int i,j,cpt0,cpt1,cptempty,cptmarks;
	stack* empty = createstack(0,sizeof(coord));//create stack to store pointers to empty grid coordinates
	cpt0 = 0;
	cpt1 = 0;
	cptmarks = 0;
	cptempty = 0;
        #pragma omp parallel for
	for(i = 0; i < g->size;i++){
        empty = createstack(0,sizeof(coord));
        
                #pragma omp parallel for
		for(j = 0; j< g->size;j++){
			if(g->grid[(i * g->size) + j] == '0'){
				cpt0++;
			}
			else if(g->grid[(i * g->size) + j] == '1'){
				cpt1++;
			}
			else{
				//trouver les duos
				if(j < g->size - 2){
					if(g->grid[(i * g->size) + j + 1] == g->grid[(i * g->size) + j + 2]){//duo sur la droite
						if(g->grid[(i * g->size)+j+1] == '0'){
							mark_case(i,j,g,'1',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								cpt1++;
                                
								
							}
							continue;
						}
						else if(g->grid[i*g->size + j + 1] == '1'){
							
							mark_case(i,j,g,'0',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt0++;
                                
							}
							continue;
						}
					}
				}
				if(j > 1){ // chercher les duo a gauche
					if(g->grid[(i * g->size) + j - 1] == g->grid[(i * g->size) + j - 2]){//duo sur la gauche
						if(g->grid[(i * g->size)+j-1] == '0'){
							
							mark_case(i,j,g,'1',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt1++;
                                
							}
							continue;
						}
						else if(g->grid[i*g->size + j - 1] == '1'){
							
							mark_case(i,j,g,'0',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt0++;
                                
							}
							continue;
						}
					}
				}
				if(j > 0 && j < g->size -1){//eradiquer les trios
					if(g->grid[(i * g->size) + j -1] == g->grid[(i * g->size)+j+1]){
						if(g->grid[(i * g->size)+j-1] == '0'){
							
							mark_case(i,j,g,'1',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt1++;
                                
							}
							continue;
						}
						else if(g->grid[i*g->size + j - 1] == '1'){
							
							mark_case(i,j,g,'0',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt0++;
                                
							}
							continue;
						}
					}
				}
				cptempty++;
				cur.i = i;
				cur.j = j;
				push(empty,&cur);
			}
		}
		
		if(cpt0 != cpt1){
            if(!Sempty(empty)) {
                
            }
			if(!Sempty(empty)&&(gettip(empty)->number + 1) == abs(cpt0-cpt1)){/*condition du même nombre de 0  et de 1 : si la différence est egale
			au  nombre de cases vides on les remplis avec le nombre voulu*/
			
				if(cpt0 > cpt1){
					while(!Sempty(empty)){
						cur = *((coord*)(pop(empty)->data));
						
						mark_case(cur.i,cur.j,g,'1',1,histo);
						cptmarks++;
						if(!check_grid(g)){
							
							if(unmark_guess(g,histo) == 0) {
								// No solution...
								define_errcg_nosolution(g);
								return;
							}
							
						}
						else if(suggest){
								nextmove->location->i = cur.i;
								nextmove->location->j = cur.j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
					}
				}
				else{
					while(!Sempty(empty)){
						cur = *((coord*)(pop(empty)->data));
						
						mark_case(cur.i,cur.j,g,'0',1,histo);
						cptmarks++;
						if(!check_grid(g)){
							
							if(unmark_guess(g,histo) == 0) {
								// No solution...
								define_errcg_nosolution(g);
								return;
							}
								
						}
						else if(suggest){
								nextmove->location->i = cur.i;
								nextmove->location->j = cur.j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
					}
				}
			}
		}
		cpt0 = 0;
		cpt1 = 0;
		
		delstack(&empty);
	}
	
        #pragma omp parallel for
	for(j = 0; j < g->size;j++){
		empty = createstack(0,sizeof(coord));
		
                #pragma omp parallel for
		for(i = 0; i< g->size;i++){
			if(g->grid[(i * g->size) + j] == '0'){
				cpt0++;
			}
			else if(g->grid[(i * g->size) + j] == '1'){
				cpt1++;
			}
			else{
				//trouver les duos
				if(i > 1){
					if(g->grid[(i-1) * g->size + j ] == g->grid[(i-2) * g->size +j ]){//duo en haut
						if(g->grid[(i-1) * g->size+j] == '0'){
							
							mark_case(i,j,g,'1',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt1++;
							}
							continue;
						}
						else if (g->grid[(i-1) * g->size + j] == '1'){
							
							mark_case(i,j,g,'0',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt0++;
							}continue;
						}
					}
				}
				if(i < g->size-2){ // chercher les duo en bas
					if(g->grid[(i+1) * g->size +j ] == g->grid[(i+2) * g->size + j]){
						if(g->grid[(i+1) * g->size+j] == '0'){
							
							mark_case(i,j,g,'1',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt1++;
							}continue;
						}
						else if (g->grid[(i+1) * g->size + j] == '1'){
							
							mark_case(i,j,g,'0',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt0++;
							}continue;
						}
					}
				}
				if(i > 0 && i < g->size -1){//eradiquer les trios
					if(g->grid[(i-1) * g->size + j ] == g->grid[(i+1) * g->size+j]){
						if(g->grid[(i-1) * g->size+j] == '0'){
							
							mark_case(i,j,g,'1',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt1++;
							}continue;
						}
						else if (g->grid[(i-1) * g->size + j] == '1'){
							
							mark_case(i,j,g,'0',1,histo);
							cptmarks++;
							if(!check_grid(g)){
								
								if(unmark_guess(g,histo) == 0) {
									// No solution...
									define_errcg_nosolution(g);
									return;
								}
								
							}
							else if(suggest){
								nextmove->location->i = i;
								nextmove->location->j = j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
							else{
								
								cpt0++;
							}continue;
						}
					}
				}
				cptempty++;
				cur.i = i;
				cur.j = j;
				push(empty,&cur);
			}
		}
		if(cpt0 != cpt1){
			if(!Sempty(empty)&&(gettip(empty)->number + 1) == abs(cpt0-cpt1)){/*condition du même nombre de 0  et de 1 : si la différence est egale
			au  nombre de cases vides on les remplis avec le nombre voulu*/
			
				if(cpt0 > cpt1){
					while(!Sempty(empty)){
						cur = *((coord*)(pop(empty)->data));
						
						mark_case(cur.i,cur.j,g,'1',1,histo);
						cptmarks++;
						if(!check_grid(g)){
							
							if(unmark_guess(g,histo) == 0) {
								// No solution...
								define_errcg_nosolution(g);
								return;
							};
							
						}
						else if(suggest){
								nextmove->location->i = cur.i;
								nextmove->location->j = cur.j;
								nextmove->value = 1;
								unmark_case(g,histo);
								goto endsolve;
							}
					}
				}
				else{
					while(!Sempty(empty)){
						cur = *((coord*)(pop(empty)->data));
						
						mark_case(cur.i,cur.j,g,'0',1,histo);
						cptmarks++;
						if(!check_grid(g)){
							
							if(unmark_guess(g,histo) == 0) {
								// No solution...
								define_errcg_nosolution(g);
								return;
							}
								
						}
						else if(suggest){
								nextmove->location->i = cur.i;
								nextmove->location->j = cur.j;
								nextmove->value = 0;
								unmark_case(g,histo);
								goto endsolve;
							}
					}
				}
			}
			else{
				
			}
		}
		
		cpt0 = 0;
		cpt1 = 0;
		
        delstack(&empty);
	}
	
	if(cptmarks == 0 && nextmove == 0) {
		mark_guess(g,histo);
	}
	cptmarks = 0;
	
    endsolve:
	
	if(!(check_grid(g) && is_fill(g))&&!suggest){
		return solve_grid(g,0,histo);
	}
}

/// Sub-function for check_grid : check rule 1-2 for each case
int check_this_case(char val, checkGrid_vars *cgv, int size, int i, int j, int inLine) {
    if(val == '0') {
        // Rule 1
        (cgv->nbr0inARow)++;
        cgv->nbr1inARow = 0;
        
        if(cgv->nbr0inARow == 3) {
            // Define errcg
            errcg = malloc(sizeof(err_checkGrid));
            errcg->msg = malloc(60 * sizeof(char));
            if(inLine) {
                sprintf(errcg->msg, "3 '0' à la suite (ligne %d, colonnes %d-%d)", i, j-2, j);
				errcg->xBeg = i;
				errcg->xEnd = i;
				errcg->yBeg = j-2;
				errcg->yEnd = j;
            } else {
                sprintf(errcg->msg, "3 '0' à la suite (lignes %d-%d, colonne %d)", j-2, j, i);
				errcg->xBeg = j-2;
				errcg->xEnd = j;
				errcg->yBeg = i;
				errcg->yEnd = i;
            }
            errcg->inLine = inLine;
            
            return 0;
        }
        // Rule 2
        (cgv->nbr0inLine)++;
        if(cgv->nbr0inLine == size / 2 + 1) {
            // Define errcg
            errcg = malloc(sizeof(err_checkGrid));
            errcg->msg = malloc(60 * sizeof(char));
            if(inLine) {
                sprintf(errcg->msg, "trop de '0' sur la ligne %d", i);
				errcg->xBeg = i;
				errcg->xEnd = i;
				errcg->yBeg = 0;
				errcg->yEnd = size-1;
            } else {
                sprintf(errcg->msg, "trop de '0' sur la colonne %d", i);
				errcg->xBeg = 0;
				errcg->xEnd = size-1;
				errcg->yBeg = i;
				errcg->yEnd = i;
            }
            errcg->inLine = inLine;
            
            return 0;
        }
        // Rule 3
        *(cgv->hashLines + i) += 1;
    } else if(val == '1') {
        // Rule 1
        cgv->nbr0inARow = 0;
        (cgv->nbr1inARow)++;
        if(cgv->nbr1inARow == 3) {
            // Define errcg
            errcg = malloc(sizeof(err_checkGrid));
            errcg->msg = malloc(60 * sizeof(char));
            if(inLine) {
                sprintf(errcg->msg, "3 '1' à la suite (ligne %d, colonnes %d-%d)", i, j-2, j);
				errcg->xBeg = i;
				errcg->xEnd = i;
				errcg->yBeg = j-2;
				errcg->yEnd = j;
            } else {
                sprintf(errcg->msg, "3 '1' à la suite (lignes %d-%d, colonne %d)", j-2, j, i);
				errcg->xBeg = j-2;
				errcg->xEnd = j;
				errcg->yBeg = i;
				errcg->yEnd = i;
            }
            errcg->inLine = inLine;

            return 0;
        }
        // Rule 2
        (cgv->nbr1inLine)++;
        if(cgv->nbr1inLine == size / 2 + 1) {
            // Define errcg
            errcg = malloc(sizeof(err_checkGrid));
            errcg->msg = malloc(60 * sizeof(char));
            if(inLine) {
                sprintf(errcg->msg, "trop de '1' sur la ligne %d", i);
				errcg->xBeg = i;
				errcg->xEnd = i;
				errcg->yBeg = 0;
				errcg->yEnd = size-1;
            } else {
                sprintf(errcg->msg, "trop de '1' sur la colonne %d", i);
				errcg->xBeg = 0;
				errcg->xEnd = size-1;
				errcg->yBeg = i;
				errcg->yEnd = i;
            }
            errcg->inLine = inLine;

            return 0;
        }
        // Rule 3
        *(cgv->hashLines + i) += 2;
    } else { // val == '_'
        // Rule 1
        cgv->nbr0inARow = 0;
        cgv->nbr1inARow = 0;
        // Rule 2
            // Nothing
        // Rule 3
        *(cgv->hashLines + i) = -1; // This line cannot be the same with another anymore
    }
    
    *(cgv->hashLines + i) *= 3;

    return 1;
}

/// Sub-function for check_grid : check rule 3 for each line/column
int check_this_line(checkGrid_vars *cgv, int size, int i, int inLine) {
    // Vars
    int j;
    
    // Rule 3
    #pragma omp parallel for
    for (j = 0; j < i; j++) {
        if(*(cgv->hashLines + i) >= 0 && *(cgv->hashLines + j) >= 0) {
            if(*(cgv->hashLines + i) == *(cgv->hashLines + j)) {
                // Define errcg
                errcg = malloc(sizeof(err_checkGrid));
                errcg->msg = malloc(60 * sizeof(char));
                if(inLine) {
                    sprintf(errcg->msg, "Les lignes %d et %d sont identiques", j, i);
					errcg->xBeg = j;
					errcg->xEnd = i;
					errcg->yBeg = 0;
					errcg->yEnd = size-1;
                } else {
                    sprintf(errcg->msg, "Les colonnes %d et %d sont identiques", j, i);
					errcg->xBeg = 0;
					errcg->xEnd = size-1;
					errcg->yBeg = j;
					errcg->yEnd = i;
                }
                errcg->inLine = inLine;
                
                return 0;
            }
        }
    }
    
    return 1;
}

/// Test if a case cannot be marked (0 or 1 implies both an error)
int check_impossible_mark(grid *g) {
	// Vars
	int i,j;
	int size = g->size;
	char cTmp;
	
	#pragma omp parallel for
	for(i = 0, cTmp = g->grid; i < size; i++) {
        #pragma omp parallel for
        for (j = 0; j < size; j++, cTmp++) {
			// Mark '0' if empty case, then check grid.
			// FALSE -> Mark '1', then check grid.
			// FALSE -> ERROR : This case cannot be marked
			if(*((g->grid)+(g->size)*i+j) == '_') {
				mark_case(i,j,g,'0',0,grid_history);
				if(!check_grid(g)) {
					unmark_case(g,grid_history);
					mark_case(i,j,g,'1',0,grid_history);
					if(!check_grid(g)) {
						// ERROR : a case cannot be marked
						unmark_case(g,grid_history);
						// clear old errcg
						if(errcg != 0) {
							free(errcg->msg);
							free(errcg);
							errcg = 0;
						}
						// Define errcg
						errcg = malloc(sizeof(err_checkGrid));
						errcg->msg = malloc(60 * sizeof(char));
						sprintf(errcg->msg, "Impossible de marquer la case (%d:%d)", i, j);
						errcg->xBeg = i;
						errcg->xEnd = i;
						errcg->yBeg = j;
						errcg->yEnd = j;
						errcg->inLine = 0;
						return 0;
					}
					unmark_case(g,grid_history);
				} else {
					unmark_case(g,grid_history);
				}
			}
		}
	}
	
	// All cases can be marked : OK !
	return 1;
}

int check_resolvable_grid(grid *g) {
	grid *gCopy = copy_grid(0,g);
	// Create another history stack for gCopy
	 // Root item : NEVER USED
    item_history item;
    
    item.x = -1;
    item.y = -1;
    item.isLogical = 1;
    item.cOld = 'z';
    item.cNew = 'z';
    stack *histoCopy = createstack(&item, sizeof(item_history));
	
	
	solve_grid(gCopy,0,histoCopy);
	
	// errcg defined here if there is no solution
	if(errcg != 0)
		return 0;
	
	
	// There is a solution for g : OK !
	del_grid(&gCopy);
	return 1;
}

// Refactoring to solve_grid (16 times the same code)
void define_errcg_nosolution(grid *g) {
	// No solution...
	if(errcg != 0) {
		free(errcg->msg);
		free(errcg);
		errcg = 0;
	}
	
	errcg = malloc(sizeof(err_checkGrid));
	errcg->msg = malloc(25 * sizeof(char));
	
	sprintf(errcg->msg, "Grille non résolvable");
	errcg->xBeg = 0;
	errcg->xEnd = g->size-1;
	errcg->yBeg = 0;
	errcg->yEnd = g->size-1;
	
	return;
}
