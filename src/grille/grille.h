#ifndef GRILLE_HEADER
#define GRILLE_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include <omp.h>
#include "lists.h"
#include <math.h>

/// Contain all vars needed in check_this_*() functions when check grid
typedef struct {
	/** \name nbrInARow
	 * Check Rule 1 : No "000" or "111" in the grid, i.e. 3 signs in a ro
	 */
	/// @{
    int nbr0inARow;
    int nbr1inARow;
	/// @}
    
	/** \name nbrInALine
	 * Check Rule 2 : No more than (size / 2) per line
	 */
	/// @{
    int nbr0inLine;
    int nbr1inLine;
	/// @}
    
	/** \name hashLines
	 * Check No more than (size / 2) per line
	 */
	/// @{
    int *hashLines;
	/// @}
} checkGrid_vars;

/// Inform why grid isn't good
typedef struct {
	char *msg;		///< Explicit message if check_grid detected something
	
	/** \name errcg_Area
	  * Explicit the area where the grid is
	  * not good : that's all point in the rectangle with
	  * top-left corner in (xBeg:yBeg) and bottom-right
	  * corner in (xEnd:yEnd)
	  */
	/// @{
	int xBeg;
	int yBeg;
	int xEnd;
	int yEnd;
	/// @}
	
	int inLine;		/**< 1: error when parse per line
					 0: ... per column*/
} err_checkGrid;

/// Info about the last tip : that's almost err_checkGrid
typedef struct tip_grid {
	char *msg;
	int x;
	int y;
} tip_grid;

/// Item of history stack
typedef struct {
    int x;
    int y;
    int isLogical; 	///< 1: logical mark | 0: supposed mark
    char cOld;
    char cNew;
} item_history;

///coordinate structure using indices in the grid
typedef struct coord {
	int i;
	int j;
} coord;

/// @deprecated Almost the same than grid_move : value is a char instead of a integer
typedef struct square {
	coord location;
	char value;
} square;

///structure used for suggestions, contains a coord and a value as int, it seems like we had some coordination issue on this one, redundant!
typedef struct grid_move {
	coord* location;
	int value;
} grid_move;

/// Global variable which inform why grid isn't good
err_checkGrid *errcg;
/// Global variable which contain a tip to solve the gris
tip_grid *tipgr;
stack *grid_history;

int check_grid(grid *g);

// Handle history
int init_history();
int push_history(stack *histo, item_history h);
item_history last_mark();
item_history pop_history(stack *histo);
int del_history();
void solve_grid(grid*,grid_move*,stack*);///<if coord* == 0 then solve the grid, else return the next best move

int is_fill(grid *g);
coord first_empty_case(grid *g);

// Private

void mark_case(int x, int y, grid *g, char val, int isLogical, stack *histo);
int unmark_case(grid *g, stack *histo);

void mark_guess(grid *g, stack *histo);
int unmark_guess(grid *g, stack *histo);

int check_this_case(char val, checkGrid_vars *cgv, int size, int i, int j, int inLine);
int check_this_line(checkGrid_vars *cgv, int size, int i, int inLine);
int check_impossible_mark(grid *g);
int check_resolvable_grid(grid *g);

void define_errcg_nosolution(grid *g);
#endif // GRILLE_HEADER
