#include "options.h"
void retrieve_options(optstruct* opt,char* filename){
///Fonction qui parse le fichier binero.rc et remplis une structure optstruct avec l'output

/**
Cette fonction est à géométrie variable, plus on veux rajouter d'options plus on la rallong en utilisant le modèle actuel
il est important de noter que opt est une unique variable globale qui est utilisée tout le long du programme, cette fonction
ayant été écrite avant que cette disposition soit prise elle utilise un pointeur
*/
	FILE* fp = fopen(filename,"r");
	char c;
	char optstring[] = "MUSIC";
	char* tmp = (char*)malloc(strlen(optstring)*sizeof(char));
	int i;
	while(!feof(fp)){
		c = fgetc(fp);
		if(c == '#'){
			while(c != '\n'){
				c = fgetc(fp);
			}
			continue;
		}
		else{
			i = 0;
			tmp[i] = c;
			i++;
			while(i < strlen(optstring)){
				tmp[i] = fgetc(fp);
				i++;
			}
			tmp[i+1] = '\0';
			if(strcmp(tmp,optstring)==0){
				fgetc(fp);
				c = fgetc(fp);
				switch(c){
					case 'N':
						opt->mstatus = NO;
						break;
					case 'Y':
						opt->mstatus = YES;
						break;
					case 'R':
						opt->mstatus = RANDOM;
						break;
					default:
						opt->mstatus = YES;
						break;
				}
				while(c != '\n'){
					c = fgetc(fp);
				}
				continue;
			}
		}
	}
	opt->intro_played = 0;
}
