#ifndef OPTIONS_HEADER
#define OPTIONS_HEADER
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
typedef enum music{NO/** no music at all*/, YES/**semi random music using folders in mp3*/, RANDOM/** completely random music, any music can be played any time*/} music;///< \enum music les enum de ce type sont à utiliser pour rendre la structure optstruct plus lisible
typedef struct optstruct{
	music mstatus;
	int intro_played;
} optstruct;

extern optstruct optstatus;

void retrieve_options(optstruct* opt,char* filename);
#endif
