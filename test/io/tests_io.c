#include <stdio.h>
#include <stdlib.h>
#include "io.h"
#include <assert.h>

void test_make_grid(){
	grid* magrille = 0;
	magrille = make_grid(L);
	assert(magrille != 0);
	assert(magrille->size == L);
	int i,j;
	char c;
	for(i = 0; i < L;i++){
		for(j = 0; j < L;j++){
			c = magrille->grid[i+j];
			assert(c == '_');
		}
	}
	assert(magrille->del == &del_grid);
	magrille->del(&magrille);
	assert(magrille == 0);
}

void test_del_grid(){
	grid* magrille = make_grid(2);
	assert(magrille != 0);
	del_grid(&magrille);
	assert(magrille == 0);
}

void io_run_all_tests(){
	test_make_grid();
	test_del_grid();
}

