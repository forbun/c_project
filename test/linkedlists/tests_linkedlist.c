#include "lists.h"
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>

int sup(int* a, int* b){
	return (*a > *b);
}

int inf(int* a, int*b){
	return (*a < *b);
}

int run_linkedlists_tests(){
	
	int j;
	int l = 1;
	int m = 2;
	assert(sup(&m,&l));
	assert(inf(&l,&m));
	linkedlist* swaplist = createlist(0,sup,inf,sizeof(int));
	int i;
	for(i = 0; i<6;i++){
		additem(swaplist,&i);
	}
	swap(getitem(swaplist,0),getitem(swaplist,5));
	assert(getlast(swaplist)->number == 5);
	assert(getfirst(swaplist)->number == 0);
	assert(*((int*)(getlast(swaplist)->data)) == 0);
	assert(getlast(swaplist)->next == 0);
	assert(getfirst(swaplist)->prev == 0);
	assert(*((int*)(getfirst(swaplist)->data)) == 5);
	swap(getitem(swaplist,1),getitem(swaplist,2));
	assert(*((int*)(getitem(swaplist,1))->data) == 2);
	linkedlist* emptylist = createlist(0,0,0,sizeof(int));
	assert(emptylist->element == 0);
	assert(emptylist->sup == 0);
	i=1;
	linkedlist* mylist = createlist(&i,sup,inf,sizeof(int)); 
	assert(mylist != 0); //mylist has been allocated
	assert(mylist->element != 0);//initialisation correctness
	assert(mylist->sup != 0);
	assert(getitem(mylist,0) != 0);// verify that its not empty
	item* testitem = getitem(mylist,0);
	assert(getitem(mylist,0)!=0);
	assert(*((int*)(testitem->data)) == 1);//verify the item has been added
	i = 2;
	additem(mylist,&i);
	testitem = getitem(mylist,1);
	assert(*((int*)(testitem->data))==2);//idem for another item
	assert(*((int*)(testitem->prev->data))==1);//idem for another item
	assert(*((int*)(testitem->prev->next->data))==2);//idem for another item
	assert(mylist->sup != 0);
	assert(mylist->sup(getitem(mylist,1)->data,getitem(mylist,0)->data) == 1);
	assert(mylist->sup(getitem(mylist,0)->data,getitem(mylist,1)->data) == 0);
	i = 3;
	item* test2 = additem(mylist,&i);
	//lets test for linking
	assert(test2->prev = testitem);
	assert(testitem->next = test2);
	swap(getitem(mylist,1),getitem(mylist,0));
	assert(*((int*)(getitem(mylist,1)->data))==1);
	assert(*((int*)(getitem(mylist,0)->data))==2);
	assert(getitem(mylist,0)->prev == 0);
	item* firstitem = getitem(mylist,0);
	assert(getitem(mylist,1)->prev == getitem(mylist,0));
	assert(getitem(mylist,1)->next == getitem(mylist,2));
	i = 0;
	item* firstelt = getitem(mylist,0);
	assert(firstelt != 0);
	item* doe = insert(getitem(mylist,0),0,&i,mylist);//insert a 0 valued item at the start of the list
	assert(firstelt->prev = doe);
	assert(doe->next = firstelt);
	assert(doe != 0);
	assert(doe->number == 0);
	assert(*((int*)(doe->data)) == 0);
	assert(doe->prev == 0);
	assert(doe->next == firstitem);
	i = 4;
	testitem = 0;
	testitem = insert(getitem(mylist,3),1,&i,mylist);//insert 
	assert(testitem != 0);
	assert(*((int*)(testitem->data)) == 4);
	assert(testitem->next == 0);
	assert(testitem->prev = test2);
	assert(test2->next = testitem);
	assert(doe == getitem(mylist,0));
	assert(*((int*)(getitem(mylist,0)->data)) == 0);
	assert(*((int*)(getitem(mylist,4)->data)) == 4);
	item* controlitem = getitem(mylist,0);
	for(i = 0; i < 4; i++){
		assert(getitem(mylist,i) == controlitem);
		controlitem = controlitem->next;
		assert(getitem(mylist,i)->next = controlitem);
		if(controlitem != 0){
			assert(controlitem->prev = getitem(mylist,i));
		}
		assert(getitem(mylist,i)->number == i);
		assert(getitem(mylist,i)->next == getitem(mylist,i+1));
		if(i>0){
			assert(getitem(mylist,i)->prev == getitem(mylist,i-1));
		}
	}
	//here add assertion to verify linking in both directions
	i = 3;
	item* john = 0;
	john = createItem(&i,sizeof(i));
	assert(john != 0);
	assert(john->data !=&i);
	assert(*((int*)(john->data)) == i);
	assert(john->next == 0);
	assert(john->prev == 0);
	int* k = delitemByNum(mylist,0);
	assert(*k == 0);
	assert(*((int*)(getitem(mylist,0)->data)) == 2);
	k = delitemByNum(mylist,1);
	assert(*k == 1);
	assert(*((int*)(getitem(mylist,1)->data)) == 3);
	insert(getitem(mylist,3),1,&i,mylist);
	i = 3;
	linkedlist* newlist = createlist(&i,sup,inf,sizeof(int));
	i = 2;
	insert(newlist->element,1,&i,newlist);
	i = 1;
	insert(newlist->element,1,&i,newlist);
	dellist(&mylist);
	assert(mylist == 0);
	mylist = createlist(&i,sup,inf,sizeof(int));
	delitemByNum(mylist,0);
	assert(Lempty(mylist) == 1);
	dellist(&mylist);

	mylist = createlist(0,0,0,sizeof(int));
	newlist = createlist(0,0,0,sizeof(int));

	for(i = 0; i < 4; i++){
		assert(additem(mylist,&i)->number == i);
	}
	for(i = 4; i < 8; i++){
		assert(additem(newlist,&i)->number == i-4);
	}
	listFusion(&mylist,&newlist);
	testitem = mylist->element;
	for(i = 0; i < 8; i++){
		assert(*((int*)(testitem->data)) == i);
		assert(testitem->number == i);
		if(i != 0){
			assert(*((int*)(testitem->prev->data)) == i-1);
			assert(testitem->prev->next == testitem);
		}
		if(i != 7){
			assert(*((int*)(testitem->next->data)) == i+1);
			assert(testitem->next->prev ==testitem);
		}
		testitem = testitem->next;
	}
	dellist(&newlist);
	newlist = listCpy(mylist);
	item* cur = getfirst(mylist);
	testitem = getfirst(newlist);
	while(cur != 0){
		assert(*((int*)(cur->data)) == *((int*)(testitem->data)));
		assert(cur->number == testitem->number);
		cur = cur->next;
		testitem = testitem->next;
	}
	dellist(&newlist);
	newlist = createlist(0,0,0,sizeof(int));
	for(i = 0;i<6;i++){
		additem(newlist,&i);
	}
	mylist = listSplit(newlist,getitem(newlist,3));
	for(i = 0;i<3;i++){
		assert(*((int*)(getitem(newlist,i)->data))==*((int*)(getitem(mylist,i)->data)) -3);
	}
	dellist(&mylist);
	dellist(&newlist);
	mylist = createlist(0,sup,inf,sizeof(int));
	for(i = 0; i<6;i++){
		additem(mylist,&i);
	}
	heapify(mylist);
	for(i = 5; i>0;i--){
		assert(getitem(mylist,i)->number ==i);
		assert(*((int*)(getitem(mylist,i/2)->data)) >= *((int*)(getitem(mylist,i)->data)));
	}
	dellist(&mylist);
	i = 0;
	newlist = createlist(&i,sup,inf,sizeof(int));
	for(i = 1;i<6;i++){
		additem(newlist,&i);
	}
	introsort(newlist);
	cur = getfirst(newlist);
	i = 5;
	while(cur != 0){
		assert(*((int*)(cur->data)) == i);
		cur = cur->next;
		i--;
	}
	i = 7;
	assert(quickselect(newlist,&i) == 0);
	i = 0;
	assert(quickselect(newlist,&i) == getlast(newlist));

	dellist(&newlist);
	newlist = createlist(0,sup,inf,sizeof(int));
	j = 25;
	for(i = 0; i <10; i++){
		if(i < 5){
			additem(newlist,&i);
		}
		else{
			additem(newlist,&j);
		}
	}

	mylist = listCpy(newlist);
	linkedlist* thirdlist = listCpy(newlist);
	assert(delitemByData(newlist,&j,0)==5);
 	dellist(&newlist);
	assert(delitemByData(mylist,&j,3) == 3);
	testitem = getfirst(mylist);
	i = 0;
	while(testitem != 0){
		if(*((int*)(testitem->data)) == 25){
			i++;
		}
		testitem = testitem->next;
	}
	assert(i == 2);
	i = 25;
	assert(delitemByData(thirdlist,&i,35)==5);

	
//les piles
	i = 1;
	stack* monstack = createstack(&i,sizeof(int));
	assert(monstack != 0);
	assert(*((int*)(gettip(monstack)->data)) == 1);
	i = 2;
	assert(push(monstack,&i) != 0);
	assert(*((int*)(monstack->tip->data)) == 2);
	testitem = 0;
	testitem= pop(monstack);
	assert(*((int*)(monstack->tip->data)) == 1);
	assert(*((int*)(testitem->data)) == 2);
	pop(monstack);
	assert(Sempty(monstack)==1);
	delstack(&monstack);
	assert(monstack == 0);
	
	monstack = createstack(0,sizeof(int));
	stack* otherstack = createstack(0,sizeof(int));

	for(i = 0; i < 4; i++){
		push(monstack,&i);
	}
	for(i = 4; i < 8; i++){
		push(otherstack,&i);
	}
	stackFusion(monstack,otherstack);
	testitem = monstack->list->element;
	while(testitem->prev != 0){
		testitem == testitem->prev;
	}
	i = 0;
	while(testitem->next != 0){
		assert(testitem->number == i);
		i++;
		testitem = testitem->next;
	}
	assert(*((int*)(monstack->tip->data)) == 7);
	for(i = 7; i >= 0; i--){
		testitem = pop(monstack);
		assert(*((int*)(testitem->data)) == i);
		if(i > 0){
			assert(*((int*)(monstack->tip->data)) == i-1);
		}
		free(testitem);
	}
	delstack(&monstack);
	monstack = createstack(0,sizeof(int));
	for(i = 0; i < 4; i++){
		push(monstack,&i);
	}
	stack* newstack = stackCpy(monstack);
	for(i = 3; i >= 0; i--){
		testitem = pop(newstack);
		assert(*((int*)(testitem->data)) == i);
	}
	delstack(&newstack);
	delstack(&monstack);
	monstack = createstack(0,sizeof(int));
	for(i = 0; i<6;i++){
		push(monstack,&i);
	}
	
	newstack = stackSplit(monstack,getitem(monstack->list,3));
	for(i = 2; i>=0; i--){
		assert(*((int*)(monstack->tip->data)) == *((int*)(newstack->tip->data)) - 3);
		pop(monstack);
		pop(newstack);
	}
	//queues
	i = 0;
	queue* myQ = createQueue(&i,sup,sizeof(int));
	i = 1;
	assert(*((int*)(myQ->head->data)) == 0);
	assert(*((int*)(myQ->tail->data)) == 0);
	assert(myQ->head == myQ->tail);

	testitem = enqueue(myQ,&i);
	assert(myQ->tail == testitem);
	assert(myQ->tail->prev == myQ->head);
	assert(myQ->head->next == testitem);
	assert(myQ->head->next == myQ->tail);
	j = getlast(myQ->list)->number;
	testitem = dequeue(myQ);
	assert(myQ->head->number == 0);
	assert(myQ->tail->number == j-1);
	assert(*((int*)(testitem->data)) == 0);
	assert(myQ->head == myQ->tail);
	assert(*((int*)(myQ->head->data)) == 1);
	delQueue(&myQ);
	assert(myQ == 0);

	myQ = createQueue(0,0,sizeof(int));
	assert(Qempty(myQ) == 1);
	for(i = 0; i < 4; i++){
		enqueue(myQ,&i);
	}
	queue* newQ = queueCpy(myQ);
	assert(*((int*)(myQ->head->data)) == *((int*)(newQ->head->data)));
	assert(*((int*)(myQ->tail->data))== *((int*)(newQ->tail->data)));
	for(i = 0; i < 4; i++){
		assert(*((int*)(dequeue(newQ)->data))==i);
	}
	delQueue(&myQ);
	delQueue(&newQ);
	myQ = createQueue(0,sup,sizeof(int));
	for(i = 0; i<4; i++){
		testitem =enqueue(myQ,&i);
		assert(*((int*)(testitem->data)) == i);
		assert(*((int*)(myQ->tail->data))==i);
	}
	prioritize(myQ);
	i = 3;
	while(!Qempty(myQ)){
		assert(*((int*)(dequeue(myQ)->data)) == i);
		i--;
	}
	delQueue(&myQ);
	 myQ = createQueue(0,sup,sizeof(int));
	 newQ = createQueue(0,sup,sizeof(int));
	 for(i = 0; i <5; i++){
		enqueue(myQ,&i);
		enqueue(newQ,&i);
	 }
	 queueFusion(myQ,newQ);
	 i = 0;
	 while(!Qempty(myQ)){
		assert(*((int*)(dequeue(myQ)->data)) == i);
		i++;
		if(i == 5){
			i = 0;
		}
	 }
	 delQueue(&myQ);
	 myQ = createQueue(0,sup,sizeof(int));
	 newQ = createQueue(0,sup,sizeof(int));
	 for(i = 0; i <5; i++){
		enqueue(myQ,&i);
		enqueue(newQ,&i);
	 }
	 prioritize(myQ);
	 prioritize(newQ);
	 queueFusion(myQ,newQ);
	 i = 4;
	 while(!Qempty(myQ)){
			assert(*((int*)(dequeue(myQ)->data)) == i);
			assert(*((int*)(dequeue(myQ)->data)) == i);
			i--;
		 }
	delQueue(&myQ);
	myQ = createQueue(0,sup,sizeof(int));
	queue *ndQ = createQueue(0,sup,sizeof(int));
	for(i = 0; i<5;i++){
		enqueue(myQ,&i);
		enqueue(ndQ,&i);
	}
	prioritize(ndQ);
	newQ = queueSplit(myQ,getitem(myQ->list,3));
	i = 3;
	while(!Qempty(newQ)){
		assert(*((int*)(dequeue(newQ)->data)) == i);
		i++;
	}

	delQueue(&newQ);
	i = 0;
	while(!Qempty(myQ)){
		assert(*((int*)(dequeue(myQ)->data))==i);
		i++;
	}
	delQueue(&myQ);
	myQ = queueSplit(ndQ,getitem(ndQ->list,3));
	i = 1;
	while(!Qempty(myQ)){
		assert(*((int*)(dequeue(myQ)->data)) == i);
		i--;
	}
	i = 4;
	while(!Qempty(ndQ)){
		assert(*((int*)(dequeue(ndQ)->data)) == i);
		i--;
	}
	delQueue(&myQ);
	delQueue(&ndQ);



}
