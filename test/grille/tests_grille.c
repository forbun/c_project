#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "grille.h"

grid* grille_test1() {
    // This is the grid :
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ 1 1 1 _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // Doesn't respect Rule 1 (line 4, columns 3-5)
    grid *g = make_grid(10);
    *((g->grid)+43) = '1';
    *((g->grid)+44) = '1';
    *((g->grid)+45) = '1';
    
    return g;
}
grid* grille_test2() {
    // This is the grid :
    // _ _ _ _ _ 0 _ _ _ _
    // _ _ _ _ _ 0 _ _ _ _
    // _ _ _ _ _ 1 _ _ _ _
    // _ _ _ _ _ 0 _ _ _ _
    // _ _ _ _ _ 0 _ _ _ _
    // _ _ _ _ _ 1 _ _ _ _
    // _ _ _ _ _ 0 _ _ _ _
    // _ _ _ _ _ 0 _ _ _ _
    // _ _ _ _ _ 1 _ _ _ _
    // _ _ _ _ _ 0 _ _ _ _
    // Doesn't respect Rule 2 (column 5, on line 7)
    grid *g = make_grid(10);
    *((g->grid)+5) = '0';
    *((g->grid)+15) = '0';
    *((g->grid)+25) = '1';
    *((g->grid)+35) = '0';
    *((g->grid)+45) = '0';
    *((g->grid)+55) = '1';
    *((g->grid)+65) = '0';
    *((g->grid)+75) = '0';
    *((g->grid)+85) = '1';
    *((g->grid)+95) = '0';
    return g;
}
grid* grille_test3() {
    // This is the grid :
    // _ 0 _ _ 0 0 _ 0 _ _
    // _ 1 _ _ 1 1 _ 1 _ _
    // _ 1 _ _ 1 1 _ 1 _ _
    // _ 0 _ _ _ _ _ 0 _ _
    // _ 0 _ _ 0 0 _ 0 _ _
    // _ 1 _ _ _ _ _ 1 _ _
    // _ 0 _ _ 0 0 _ 0 _ _
    // _ 1 _ _ 0 0 _ 1 _ _
    // _ 1 _ _ 1 1 _ 1 _ _
    // _ 0 _ _ 1 1 _ 0 _ _
    // Doesn't respect Rule 3 (columns 1 & 7, mustn't detect columns 4 & 5,
    // because there is still 2 '_' on lines 3 & 5)
    grid *g = make_grid(10);
    *((g->grid)+1) = *((g->grid)+7) = '0';
    *((g->grid)+11) = *((g->grid)+17) = '1';
    *((g->grid)+21) = *((g->grid)+27) = '1';
    *((g->grid)+31) = *((g->grid)+37) = '0';
    *((g->grid)+41) = *((g->grid)+47) = '0';
    *((g->grid)+51) = *((g->grid)+57) = '1';
    *((g->grid)+61) = *((g->grid)+67) = '0';
    *((g->grid)+71) = *((g->grid)+77) = '1';
    *((g->grid)+81) = *((g->grid)+87) = '1';
    *((g->grid)+91) = *((g->grid)+97) = '0';

    *((g->grid)+5) = *((g->grid)+4) = '0';
    *((g->grid)+15) = *((g->grid)+14) =  '1';
    *((g->grid)+25) = *((g->grid)+24) =  '1';
    *((g->grid)+45) = *((g->grid)+44) =  '0';
    *((g->grid)+65) = *((g->grid)+64) =  '0';
    *((g->grid)+75) = *((g->grid)+74) =  '0';
    *((g->grid)+85) = *((g->grid)+84) =  '1';
    *((g->grid)+95) = *((g->grid)+94) =  '1';
    
    return g;
}

grid* grille_test4() {
    // This is the grid :
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // _ _ _ _ _ _ _ _ _ _
    // Test if check_grid can pass a grid, and if errcg is really undefined
    grid *g = make_grid(10);
    
    return g;
}

grid* grille_test5() {
    // This is the grid :
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // 0 0 0 0 0 0 0 0 0 0
    // Test is_fill()
    grid *g = make_grid(10);
    
    int i;
    char *cTmp;
    for(i = 0, cTmp = g->grid; i < 100; i++, cTmp++) {
        *cTmp = '0';
    }
    
    return g;
}

grid* grille_test6() {
/*
0 _
1 _
*/
	grid *g = make_grid(2);
	g->grid[0] = '0';
	g->grid[1] = '_';
	g->grid[2] = '1';
	g->grid[3] = '_';
	return g;
}

void test_solve_grid() {
	init_history();
	grid* g = grille_test6();
	solve_grid(g,0,grid_history);
	assert(is_fill(g)&&check_grid(g));//test la regle 1 sur le nombre de 0 et de 1 par ligne
	
	/*test suivant utilise le fichier test_grid*/
	FILE* fp = fopen("testgrid","r");
	grid* grille = (grid*)malloc(sizeof(grid));
	grille->del = del_grid;//in memory location of the del function might have changed
	grille->size = L;
	grille->grid = (char*)malloc(grille->size * grille->size * sizeof(char));//idem pour l'emplacement du tableau de char
	fread(grille->grid,sizeof(char),grille->size*grille->size,fp);
	fclose(fp);
	solve_grid(grille,0,grid_history);
	assert(is_fill(grille)&&check_grid(grille));
    
	// 3e test : pour le marquage a l'intuition
	fp = fopen("testgrid_2","r");
	grid* grille2 = (grid*)malloc(sizeof(grid));
	grille2->del = del_grid;//in memory location of the del function might have changed
	grille2->size = L;
	grille2->grid = (char*)malloc(grille2->size * grille2->size * sizeof(char));//idem pour l'emplacement du tableau de char
	fread(grille2->grid,sizeof(char),grille2->size*grille2->size,fp);
	fclose(fp);
	solve_grid(grille2,0,grid_history);
	assert(is_fill(grille2)&&check_grid(grille2));

}
void test_check_grid() {
    grid *g = 0;
    int check_code = -1;
    
    // Test rule 1
    g = grille_test1();
    assert(g != 0);
    check_code = check_grid(g);
    assert(check_code == 0);
    assert(errcg != 0);
    assert(strcmp(errcg->msg, "3 '1' à la suite (ligne 4, colonnes 3-5)") == 0);
    del_grid(&g);
    g = 0;
    
    // Test Rule 2
    g = grille_test2();
    assert(g != 0);
    check_code = check_grid(g);
    assert(check_code == 0);
    assert(errcg != 0);
    assert(strcmp(errcg->msg, "trop de '0' sur la colonne 5") == 0);
    del_grid(&g);
    g = 0;
    
    // Test Rule 3
    g = grille_test3();
    assert(g != 0);
    check_code = check_grid(g);
    assert(check_code == 0);
    assert(errcg != 0);
    assert(strcmp(errcg->msg, "Les colonnes 1 et 7 sont identiques") == 0);
    del_grid(&g);
    g = 0;
    
    // Test errcg, if it is really undefined when a grid is good
    g = grille_test4();
    assert(g != 0);
    check_code = check_grid(g);
    assert(check_code == 1);
    assert(errcg == 0);
    del_grid(&g);
    g = 0;
}

void test_is_fill() {
    grid *g = 0;
    int is_fill_code = -1;
    
    // Not fill
    g = grille_test4();
    assert(g != 0);
    is_fill_code = is_fill(g);
    assert(is_fill_code == 0);
    del_grid(&g);
    g = 0;
    
    // Fill
    g = grille_test5();
    assert(g != 0);
    is_fill_code = is_fill(g);
    assert(is_fill_code == 1);
    del_grid(&g);
    g = 0;
}

void test_first_empty_case() {
    grid *g = 0;
    coord c_res;
    
    // Not fill
    g = grille_test4();
    assert(g != 0);
    c_res = first_empty_case(g);
    assert(c_res.i == 0 && c_res.j == 0);
    del_grid(&g);
    g = 0;
    
    // Fill
    g = grille_test5();
    assert(g != 0);
    c_res = first_empty_case(g);
    assert(c_res.i == -1 && c_res.j == -1);
    del_grid(&g);
    g = 0;
}

void test_history() {
    init_history();
    
    item_history test1, test2, testH;
    test1.x = 3;
    test1.y = 6;
    test1.cOld = '_';
    test1.cNew = '0';
    test2.x = 7;
    test2.y = 1;
    test2.cOld = '1';
    test2.cNew = '_';
    
    // Test push / pop functions
    push_history(grid_history,test1);
    push_history(grid_history,test2);
    
    testH = pop_history(grid_history);
    // testH == test2
    assert(testH.x = 7);
    assert(testH.y = 1);
    assert(testH.cOld = '1');
    assert(testH.cNew = '_');
    
    testH = pop_history(grid_history);
    // testH == test1
    assert(testH.x = 3);
    assert(testH.y = 6);
    assert(testH.cOld = '_');
    assert(testH.cNew = '0');
    
    del_history();
}

void test_copy_grid() {
	grid *src, *dest;
	
	// Create dest grid
	dest = grille_test3();
	
	src = copy_grid(0,dest);
	
	assert(strcmp("", dest->grid) != 0);
	assert(strcmp(src->grid, dest->grid) == 0);
	
	// Free ressources
	del_grid(&src);
	del_grid(&dest);
}

void grille_run_all_tests() {
    test_check_grid();
    test_is_fill();
    test_first_empty_case();
    test_history();
    test_solve_grid();
}
