#include "options.h"
#include "sounds.h"
mus soundenv;
optstruct optstatus;
int main(){
	io_run_all_tests();
	run_linkedlists_tests();
	io_lib_tests();
	grille_run_all_tests();
	interface_run_all_tests();
	return 0;
}
