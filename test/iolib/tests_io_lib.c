#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "iolib.h"
void test_nbdigits(){
	int i = 100;
	assert(nbdigits(i) == 3);
}

void io_lib_tests(){
	test_nbdigits();
}
