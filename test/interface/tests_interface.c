#define _X_OPEN_SOURCE_EXTENDED

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <locale.h>
#include <ncursesw/ncurses.h>

#include "interface.h"
#include "grille.h"
#include "sounds.h"

void interface_run_all_tests() {
	mus soundenv;
	optstruct optstatus;
	setlocale(LC_ALL, "");
	
	FILE *fp;
	grid* grille;
	
	initscr();      /* initialize the curses library */
	keypad(stdscr, TRUE);  /* enable keyboard mapping */
	raw();
	nonl();         /* tell curses not to do NL->CR/NL on output */
	cbreak();
	noecho();
	
	fp = fopen("testgrid_2","r");
	grille = (grid*)malloc(sizeof(grid));
	grille->del = del_grid;//in memory location of the del function might have changed
	grille->size = L;
	grille->grid = (char*)malloc(grille->size * grille->size * sizeof(char));//idem pour l'emplacement du tableau de char
	fread(grille->grid,sizeof(char),grille->size*grille->size,fp);
	fclose(fp);
	
	// Step 1 : static grid
	print_grid(stdscr, grille);
	wrefresh(stdscr);
	getch();
	wclear(stdscr);
	
	// Step 2 : static grid, offset
	print_grid_offset(stdscr, 3, 50, grille);
	wrefresh(stdscr);
	getch();
	wclear(stdscr);
	
	// Step 3 : nav grid, return coord
	grid_move arr;
	arr.location = malloc(sizeof(coord));
	arr.location->i = 0;
	arr.location->j = 0;
	arr = nav_grid(stdscr, 10, 50, arr.location, grille);
	// Show the result
	wmove(stdscr,0,50);
	wrefresh(stdscr);
	wprintw(stdscr, "arr: (%d,%d) valKB(int):%d", (arr.location)->i, (arr.location)->j, arr.value, arr.value);
	getch();
	
	endwin();
}
